const exp = require('express')
var studentroutes = exp.Router()
const initDb = require('../configdb').initDb
const getDb = require('../configdb').getDb
const bodyparser = require('body-parser')
const checkauthorization = require('../middleware/authorization')
studentroutes.use(bodyparser.json())
initDb()
studentroutes.get('/readattd', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection("saveattd").find().toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {

      res.json({ message: dataArray })
    }

  })

}
)
studentroutes.get('/readfee', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection("savefee").find().toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {

      res.json({ message: dataArray })
    }

  })

}
)
studentroutes.get('/readmark', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection("savemark").find().toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {

      res.json({ message: dataArray })
    }

  })

}
)
studentroutes.post('/viewspecificreq', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection("rescollection").find({ rollnumber: { $eq: req.body.rollnumber } }).toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: dataArray })
    }

  })

}
)

studentroutes.post('/savereq', checkauthorization, (req, res, next) => {
  dbo = getDb()
  if (req.body == {}) {
    res.json({ message: "server did not receive data" })
  }
  else {
    dbo.collection("reqcollection").insertOne(req.body, (err, dataArray) => {
      if (err) {
        next(err)
      }
      else {
        dbo.collection("rescollection").deleteOne({ rollnumber: { $eq: req.body.rollnumber } }, (err, dataArray) => {
          if (err) {
            next(err)
          }
          else {
            res.json({ message: "Request sent to Admin" })
          }
        })

      }
    })
  }

})
studentroutes.post('/viewspecificmarks', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection("savemark").find({ rollnumber: { $eq: req.body.rollnumber } }).toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: dataArray })
    }

  })

}
)


studentroutes.post('/viewspecificstd', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection("save").find({ rollnumber: { $eq: req.body.rollnumber } }).toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: dataArray })
    }

  })

}
)


studentroutes.post('/viewspecificfees', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection("savefee").find({ rollnumber: { $eq: req.body.rollnumber } }).toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: dataArray })
    }

  })

}
)
studentroutes.post('/viewspecificattd', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection("saveattd").find({ rollnumber: { $eq: req.body.rollnumber } }).toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: dataArray })
    }

  })

}
)
studentroutes.get('/readnote', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection("nsavenotify").find().toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: dataArray })
    }

  })

}
)
studentroutes.use((err, req, res, next) => {
  console.log(err)
})


module.exports = studentroutes
