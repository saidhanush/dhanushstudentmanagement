const exp = require('express')
var adminroutes = exp.Router()
const initDb = require('../configdb').initDb
const getDb = require('../configdb').getDb
const bodyparser = require('body-parser')
const bcrypt = require('bcrypt')
const nodemailer = require('nodemailer')
adminroutes.use(bodyparser.json())
const jwt = require('jsonwebtoken')
const secretkey = "secret"
const checkauthorization = require('../middleware/authorization')
const accountSid = 'AC806bce520b739550eb92a957e8744f61';
const authToken = '32ac17dc586964a7623e278b4635635d';
const client = require('twilio')(accountSid, authToken);


initDb()
adminroutes.post('/save', checkauthorization, (req, res, next) => {
  let transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: 'saidhanush1998@gmail.com',
      pass: 'xxxxxxxxxxx',
    },
  });
  let info = transporter.sendMail({
    // sender address
    from: '"admin " <saidhanush1998@gmail.com>',
    // list of receivers
    to: req.body.gmail,
    subject: "student credentials",
    text: `rollnumber: ${req.body.rollnumber},password: ${req.body.password}`,
  });

  dbo = getDb()
  dbo.collection("save").find({ rollnumber: { $eq: req.body.rollnumber } }).toArray((err, studentArray) => {
    if (studentArray == "") {
      bcrypt.hash(req.body.password, 6, (err, hashedpassword) => {
        if (err) {
          next(err)
        }
        else {
          req.body.password = hashedpassword

          dbo.collection("save").insertOne(req.body, (err, dataArray) => {
            if (err) {
              next(err)
            }
            else {
              res.json({ message: "successfully inserted" })
            }
          })
        }

      })
    }
    else {
      res.json({ message: "student already exists with this rollnumber" })
    }
  })
})
adminroutes.post('/saveattd', checkauthorization, (req, res, next) => {
  dbo = getDb()
  if (req.body == {}) {
    res.json({ message: "server did not receive data" })
  } else {
    dbo.collection("saveattd").insertOne(req.body, (err, dataArray) => {
      if (err) {
        next(err)
      }
      else {
        res.json({ message: "successfully inserted" })
      }
    })
  }

})
adminroutes.post('/savefee', checkauthorization, (req, res, next) => {
  dbo = getDb()
  if (req.body == {}) {
    res.json({ message: "server did not receive data" })
  } else {
    dbo.collection("savefee").insertOne(req.body, (err, dataArray) => {
      if (err) {
        next(err)
      }
      else {
        res.json({ message: "successfully inserted" })
      }
    })
  }

})
adminroutes.post('/savemark', checkauthorization, (req, res, next) => {
  dbo = getDb()
  if (req.body == {}) {
    res.json({ message: "server did not receive data" })
  } else {
    dbo.collection("savemark").insertOne(req.body, (err, dataArray) => {
      if (err) {
        next(err)
      }
      else {
        res.json({ message: "successfully inserted" })
      }
    })
  }

})
adminroutes.post('/savenotify', checkauthorization, (req, res, next) => {
  dbo = getDb()
  if (req.body == {}) {
    res.json({ message: "server did not receive data" })
  } else {
    dbo.collection("nsavenotify").insertOne(req.body, (err, dataArray) => {
      if (err) {
        next(err)
      }
      else {
        res.json({ message: "successfully inserted" })
      }
    })
  }

})
adminroutes.post('/savereq', checkauthorization, (req, res, next) => {
  dbo = getDb()
  if (req.body == {}) {
    res.json({ message: "server did not receive data" })
  } else {
    dbo.collection("rescollection").insertOne(req.body, (err, dataArray) => {
      if (err) {
        next(err)
      }
      else {

        dbo.collection("reqcollection").deleteOne({ rollnumber: { $eq: req.body.rollnumber } }, (err, dataArray) => {
          if (err) {
            next(err)
          }
          else {
            res.json({ message: "successfully inserted" })
          }
        })
      }
    })
  }

})
adminroutes.get('/readstd', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection("save").find().toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: dataArray })
    }

  })

}
)
adminroutes.get('/readreq', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection("reqcollection").find().toArray((err, dataArray) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: dataArray })
    }

  })

}
)









adminroutes.post('/login', (req, res, next) => {
  dbo = getDb();
  dbo.collection('save').find({ rollnumber: { $eq: req.body.rollnumber } }).toArray((err, userArray) => {
    if (userArray.length == 0) {
      res.json({ message: "Invalid username" })
    }
    else {
      bcrypt.compare(req.body.password, userArray[0].password, (err, result) => {
        if (result == true) {
          const signedtoken = jwt.sign({ rollnumber: userArray[0].rollnumber }, secretkey, { expiresIn: "7d" })
          res.json({ message: "success", token: signedtoken, data: userArray })
        }

        else {
          res.json({ message: "Invalid password" })
        }
      })
    }
  })
})

adminroutes.post('/adminlogin', (req, res, next) => {
  dbo = getDb();
  dbo.collection('adminlogin').find({ rollnumber: { $eq: req.body.rollnumber } }).toArray((err, userArray) => {
    if (userArray.length == 0) {
      res.json({ message: "Invalid admin" })
    }
    else if (userArray[0].password !== req.body.password) {
      res.json({ message: "Invalid admin password" })
    }
    else {
      const signedtoken = jwt.sign({ rollnumber: userArray[0].rollnumber }, secretkey, { expiresIn: "7d" })
      res.json({ message: "admin success", token: signedtoken })
    }



  })
})





adminroutes.delete('/delete/:rollnumber', checkauthorization, (req, res, next) => {
  //delete document with name as "req.params.name"
  dbo = getDb()
  dbo.collection("savemark").deleteOne({ rollnumber: { $eq: req.params.rollnumber } }, (err, success) => {
    if (err) {
      next(err)
    }
    else {
      dbo.collection("savemark").find().toArray((err, dataArray) => {
        if (err) {
          next(err)
        }
        else {
          res.json({ message: "Record deleted", data: dataArray })
        }
      })
    }
  })
})



adminroutes.delete('/attend/:rollnumber', checkauthorization, (req, res, next) => {
  //delete document with name as "req.params.name"
  dbo = getDb()
  dbo.collection("saveattd").deleteOne({ rollnumber: { $eq: req.params.rollnumber } }, (err, success) => {
    if (err) {
      next(err)
    }
    else {
      dbo.collection("saveattd").find().toArray((err, dataArray) => {
        if (err) {
          next(err)
        }
        else {
          res.json({ message: "Record deleted", data: dataArray })
        }
      })
    }
  })
})


adminroutes.delete('/fee/:rollnumber', checkauthorization, (req, res, next) => {
  //delete document with name as "req.params.name"
  dbo = getDb()
  dbo.collection("savefee").deleteOne({ rollnumber: { $eq: req.params.rollnumber } }, (err, success) => {
    if (err) {
      next(err)
    }
    else {
      dbo.collection("savefee").find().toArray((err, dataArray) => {
        if (err) {
          next(err)
        }
        else {
          res.json({ message: "Record deleted", data: dataArray })
        }
      })
    }
  })
})


adminroutes.delete('/profile/:rollnumber', checkauthorization, (req, res, next) => {
  //delete document with name as "req.params.name"
  dbo = getDb()
  dbo.collection("save").deleteOne({ rollnumber: { $eq: req.params.rollnumber } }, (err, success) => {
    if (err) {
      next(err)
    }
    else {
      dbo.collection("save").find().toArray((err, dataArray) => {
        if (err) {
          next(err)
        }
        else {
          res.json({ message: "Record deleted", data: dataArray })
        }
      })
    }
  })
})


adminroutes.put('/modifyatt', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection('saveattd').update({ rollnumber: { $eq: req.body.rollnumber } }, {
    $set: {
      rollnumber: req.body.rollnumber,
      month: req.body.month, overall: req.body.overall
    }
  }, (err, success) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: "success" })
    }
  })
})


adminroutes.put('/modifymark', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection('savemark').update({ rollnumber: { $eq: req.body.rollnumber } }, {
    $set: {
      rollnumber: req.body.rollnumber,
      subject: req.body.subject, marks: req.body.marks, totalmarks: req.body.totalmarks
    }
  }, (err, success) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: "success" })
    }
  })
})







adminroutes.put('/modifyfees', checkauthorization, (req, res, next) => {
  dbo = getDb()
  dbo.collection('savefee').update({ rollnumber: { $eq: req.body.rollnumber } }, {
    $set: {
      rollnumber: req.body.rollnumber,
      totalfee: req.body.totalfee, feepaid: req.body.feepaid, feedue: req.body.feedue
    }
  }, (err, success) => {
    if (err) {
      next(err)
    }
    else {
      res.json({ message: "success" })
    }
  })
})


adminroutes.post('/resetpassword', (req, res, next) => {
  dbo = getDb()
  dbo.collection('save').find({ rollnumber: req.body.rollnumber }).toArray((err, userArray) => {
    if (err) {
      next(err)
    }
    else {
      if (userArray.length === 0) {
        res.json({ message: "user not found" })
      }
      else {

        jwt.sign({ rollnumber: userArray[0].rollnumber }, secretkey, { expiresIn: 3600 }, (err, token) => {
          if (err) {
            next(err);
          }
          else {
            var OTP = Math.floor(Math.random() * 99999) + 11111;


            client.messages.create({
              body: OTP,
              from: '+13343261223', // From a valid Twilio number
              to: '+91xxxxxxxxx',  // Text this number

            })
              .then((message) => {
                dbo.collection('OTPCollection').insertOne({
                  OTP: OTP,
                  rollnumber: userArray[0].rollnumber,
                  OTPGeneratedTime: new Date().getTime() + 15000
                }, (err, success) => {
                  if (err) {
                    next(err)
                  }
                  else {
                    res.json({
                      "message": "user found",
                      "token": token,
                      "OTP": OTP,
                      "rollnumber": userArray[0].rollnumber
                    })
                  }
                })
              });

          }

        })
      }
    }
  })
})



//verify OTP
adminroutes.post('/verifyotp', (req, res, next) => {

  console.log(new Date().getTime())
  var currentTime = new Date().getTime()
  dbo.collection('OTPCollection').find({ "OTP": req.body.OTP }).toArray((err, OTPArray) => {
    if (err) {
      next(err)
    }
    else if (OTPArray.length === 0) {
      res.json({ "message": "invalidOTP" })
    }
    else if (OTPArray[0].OTPGeneratedTime < req.body.currentTime) {
      res.json({ "message": "invalidOTP" })
    }
    else {

      dbo.collection('OTPCollection').deleteOne({ OTP: req.body.OTP }, (err, success) => {
        if (err) {
          next(err);
        }
        else {

          res.json({ "message": "verifiedOTP" })
        }
      })
    }
  })
})
adminroutes.put('/changepassword', (req, res, next) => {

  dbo = getDb()
  bcrypt.hash(req.body.password, 6, (err, hashedPassword) => {
    if (err) {
      next(err)
    } else {

      dbo.collection('save').updateOne({ rollnumber: req.body.rollnumber }, {
        $set: {
          password: hashedPassword
        }
      }, (err, success) => {
        if (err) {
          next(err)
        }
        else {
          res.json({ "message": "password changed" })
        }
      })
    }
  })

})












adminroutes.use((err, req, res, next) => {
  console.log(err)
})


module.exports = adminroutes