//install and import express
const exp = require('express')
app = exp()
//install and import path 
const path = require('path')
console.log(__dirname);
//connecting angular app with server
app.use(exp.static(path.join(__dirname, '../dist/project/')));


const adminroutes = require('./routes/adminroutes');
const studentroutes = require('./routes/studentroutes');
app.use('/admin', adminroutes)
app.use('/student', studentroutes)
//port number

app.listen(process.env.PORT || 8080, () => {
    console.log("server is running ")
}
)