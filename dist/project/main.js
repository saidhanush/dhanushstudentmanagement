(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/addstudent/addstudent.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin/adminlogin/addstudent/addstudent.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-md-3\"></div>\n            <div class=\"col-md-6\">\n                <form #ref=\"ngForm\" (ngSubmit)=\"addStudent(ref.value)\">\n                    <div class=\"form-group\">\n                        <label for=\"\">Name</label>\n                        <input type=\"text\" name=\"name\" class=\"form-control\" ngModel #ref1=\"ngModel\" required>\n                        <label *ngIf=\"ref1.invalid &&  ref1.touched\"><div class=\"text-danger\">*this field is manadatory</div></label>\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"\">Roll no</label>\n                        <input type=\"text\" name=\"rollnumber\" class=\"form-control\" ngModel #ref2=\"ngModel\" required>\n                        <label *ngIf=\"ref2.invalid && (ref2.dirty || ref2.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"\">Gmail</label>\n                        <input type=\"text\" name=\"gmail\" class=\"form-control\" ngModel #ref5=\"ngModel\" required>\n                        <label *ngIf=\"ref2.invalid && (ref2.dirty || ref2.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"\">class</label>\n                        <input type=\"text\" name=\"class\" class=\"form-control\" ngModel #ref5=\"ngModel\" required>\n                        <label *ngIf=\"ref5.invalid && (ref5.dirty || ref5.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"\" >Password</label>\n                        <input type=\"password\" name=\"password\"class=\"form-control\" ngModel #ref4=\"ngModel\" required  minlength=\"5\">\n                        <div *ngIf=\"ref4.invalid && (ref4.dirty || ref4.touched)\">\n                            <div *ngIf=\"ref4.errors.minlength\" class=\"text-danger\">*password must be atleast 5 characters long</div>\n                        </div>\n                    </div>\n                    <div class=\"text-center\">\n                        <button type=\"submit\" class=\"btn btn-success\">submit</button>\n                    </div>\n                </form>\n            </div>\n            <div class=\"col-md-3\"></div>\n        </div>\n    \n    </div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/adminlogin.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin/adminlogin/adminlogin.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <nav class=\"navbar navbar-expand-sm bg-dark navbar-dark\">\n        <a href=\"\" class=\"navbarbrand\">\n            <img src=\"assets/download.png\" height=\"50%\" width=\"25%\">\n        </a>\n        <button data-toggle=\"collapse\" data-target=\"#n\" class=\"navbar-toggler\">\n            <span class=\"navbar-toggler-icon\"></span>\n        </button>\n        <div id=\"n\" class=\"collapse navbar-collapse justify-content-end text-white\">\n    <ul class=\"navbar-nav \">\n            <li class=\"nav-item\">\n                    <a routerLink=\"studentprofile\" class=\"nav-link\">Student profile</a>\n            </li>\n            <li class=\"nav-item\">\n                    <a routerLink=\"addstudent\" class=\"nav-link\">Add Student</a>\n            </li>\n            <li class=\"nav-item\">\n                    <a routerLink=\"updatemarks\" class=\"nav-link\">Update Marks</a>\n            </li> \n        <li class=\"nav-item\">\n                <a routerLink=\"updateattendance\" class=\"nav-link\">Update Attendance</a>\n        </li> \n        <li class=\"nav-item\">\n            <a routerLink=\"updatenotification\" class=\"nav-link\">Update Notification</a>\n        </li>  \n        <li class=\"nav-item\">\n            <a routerLink=\"updatefee\" class=\"nav-link\">Update Fee</a>\n        </li> \n        <li class=\"nav-item\">\n            <a routerLink=\"receivedrequest\" class=\"nav-link\">Received request</a>\n        </li>\n        <li class=\"nav-item\">\n            <a routerLink=\"logout\" class=\"nav-link\">Logout</a>\n        </li>  \n    </ul>\n    </div>\n    </nav>\n    <router-outlet></router-outlet>\n    </div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/attendance/attendance.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin/adminlogin/attendance/attendance.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"text-center mt-2 mb-2\">\n    <label for=\"\">Search :</label>\n    <input type=\"text\" name=\"search\" placeholder=\"search by rollnumber\" [(ngModel)]=\"searchterm\">\n</div>\n<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-3\"></div>\n        <div class=\"col-md-6\">\n            <form #ref=\"ngForm\" (ngSubmit)=\"attendance(ref.value)\">\n                <div class=\"form-group\">\n                    <label >Student Roll No</label>\n                    <input type=\"text\" name=\"rollnumber\" class=\"form-control\" ngModel #ref1=\"ngModel\" required>\n                    <label *ngIf=\"ref1.invalid && (ref1.dirty || ref1.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n                </div>\n                <div class=\"form-group\">\n                        <label >Attendance For Month</label>\n                        <input type=\"number\" name=\"month\" class=\"form-control\" ngModel #ref2=\"ngModel\" required>\n                        <label *ngIf=\"ref2.invalid && (ref2.dirty || ref2.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n                </div>\n                <div class=\"form-group\">\n                        <label >Overall Attendance</label>\n                        <input type=\"number\" name=\"overall\" class=\"form-control\" ngModel #ref3=\"ngModel\" required>\n                        <label *ngIf=\"ref3.invalid && (ref3.dirty || ref3.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n                </div>\n                <div class=\"text-center\">\n                    <button type=\"submit\"  class=\"btn btn-success\">Submit</button>\n                </div>\n            </form>\n\n\n\n\n\n            \n            <div *ngIf=\"b\">\n                <form #ref=\"ngForm\"  (ngSubmit)=\"onsubmit(ref.value)\">\n                    <div>\n                        <label>Change Rollno</label>\n                        <input type=\"text\" name=\"rollnumber\" class=\"form-control\" [value]=\"objecttomodify.rollno\" [(ngModel)]=objecttomodify.rollno>\n                    </div>\n                    <div>\n                        <label>Month Attendance</label>\n                        <input type=\"text\" name=\"month\" class=\"form-control\" [value]=\"objecttomodify.month\" [(ngModel)]=objecttomodify.month>\n                    </div>\n                    <div>\n                        <label>Overall Attendance</label>\n                        <input type=\"text\" name=\"overall\" class=\"form-control\" [value]=\"objecttomodify.overall\" [(ngModel)]=objecttomodify.overall>\n                    </div>\n                    <div class=\"text-center\"><button type=\"submit\" class=\"btn btn-primary mt-3\">save</button></div>\n                </form>\n            </div>\n        </div>\n        <div class=\"col-md-3\"></div>\n    </div>\n\n</div>\n<div class=\"row mt-3\">\n    <div class=\"col-sm-12\">\n<table class=\"table table-bordered \">\n    <thead>\n        <th>Roll no</th>\n        <th>Month attendance</th>\n        <th>Overall attendance</th>\n    </thead>\n    <tr *ngFor=\"let attendance of attd | search:searchterm\">\n        <td>{{attendance.rollnumber}}</td>\n        <td>{{attendance.month}}</td>\n        <td>{{attendance.overall}}</td>\n        <button (click)=\"deleterecord1(attendance.rollnumber)\" class=\"btn btn-danger mr-2\">Delete</button>\n        <button (click)=\"editdata(attendance)\" class=\"btn btn-success\">edit</button>\n    </tr>\n\n</table>\n</div>\n</div>\n    "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/feestatus/feestatus.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin/adminlogin/feestatus/feestatus.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"text-center mt-2 mb-2\">\n    <label for=\"\">Search :</label>\n    <input type=\"text\" name=\"search\" placeholder=\"search by rollnumber\" [(ngModel)]=\"searchterm\">\n</div>\n<div class=\"container \">\n        <form #ref=\"ngForm\" (ngSubmit)=\"fee(ref.value)\" >\n          <div class=\"form-group\">\n                <label for=\"\">Roll Number:</label>\n                <input type=\"text\" name=\"rollnumber\" class=\"form-control\" ngModel #ref1=\"ngModel\" required>\n                <label *ngIf=\"ref1.invalid && (ref1.dirty || ref1.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n            </div>  \n            <div class=\"form-group\">\n                <label for=\"\">Total Fee</label>\n                <input type=\"number\" name=\"totalfee\" class=\"form-control\"  ngModel #ref2=\"ngModel\" required>\n                <label *ngIf=\"ref2.invalid && (ref2.dirty || ref2.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n            </div>  \n            <div class=\"form-group\">\n                <label for=\"\">Fee paid</label>\n                <input type=\"number\" name=\"feepaid\" class=\"form-control\" ngModel #ref3=\"ngModel\" required>\n                <label *ngIf=\"ref3.invalid && (ref3.dirty || ref3.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n            </div>\n            <div class=\"form-group\">\n                <label for=\"\">Fee Due</label>\n                <input type=\"number\" name=\"feedue\" class=\"form-control\" ngModel #ref4=\"ngModel\" required>\n                <label *ngIf=\"ref4.invalid && (ref4.dirty || ref4.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n                        </div>                    \n            <div class=\"text-center\">\n                <button type=\"submit\" class=\"btn btn-success\">submit</button>\n            </div>\n    </form>\n    <div *ngIf=\"c\">\n        <form #ref=\"ngForm\"  (ngSubmit)=\"onsubmit2(ref.value)\">\n            <div>\n                <label>Change Rollno</label>\n                <input type=\"text\" name=\"rollnumber\" class=\"form-control\"  [(ngModel)]=objecttomodify2.rollnumber>\n            </div>\n            <div>\n                <label>Total Fee</label>\n                <input type=\"number\" name=\"totalfee\" class=\"form-control\" [(ngModel)]=objecttomodify2.totalfee>\n            </div>\n            <div>\n                <label>Fee paid</label>\n                <input type=\"number\" name=\"feepaid\" class=\"form-control\"  [(ngModel)]=objecttomodify2.feepaid>\n            </div>\n            <div>\n                <label>Fee Due</label>\n                <input type=\"number\" name=\"feedue\" class=\"form-control\" [(ngModel)]=objecttomodify2.feedue>\n            </div>\n            <div class=\"text-center\"><button type=\"submit\" class=\"btn btn-primary mt-3\">save</button></div>\n        </form>\n    </div>\n\n\n<div class=\"col-md-3\"></div>\n</div>\n\n\n    <div class=\"table-bordered  mt-4\">\n    <table class=\"table table-bordered\">\n       <thead>\n         <th>Roll Number</th>  \n        <th>Total Fee</th>\n        <th>Fee paid</th>\n        <th>Fee Due</th>\n       </thead>\n       <tr *ngFor=\"let afee of fees | search:searchterm\">\n            <td>{{afee.rollnumber}}</td>\n           <td>{{afee.totalfee}}</td>\n           <td>{{afee.feepaid}}</td>\n           <td>{{afee.feedue}}</td>\n           <button (click)=\"deleterecord2(afee.rollnumber)\" class=\"btn btn-danger mr-2\">Delete</button>\n           <button (click)=\"editdata2(afee)\" class=\"btn btn-success\">edit</button>\n       </tr>\n    </table>\n</div>\n    \n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/logout/logout.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin/adminlogin/logout/logout.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>logout works!</p>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/marks/marks.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin/adminlogin/marks/marks.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"text-center mt-2 mb-2\">\n    <label for=\"\">Search :</label>\n    <input type=\"text\" name=\"search\" placeholder=\"search by rollnumber\" [(ngModel)]=\"searchterm\">\n</div>\n<div class=\"container \">\n        <form #ref=\"ngForm\" (ngSubmit)=\"markson(ref.value)\" >\n          <div class=\"form-group\">\n                <label for=\"\">Roll Number:</label>\n                <input type=\"text\" name=\"rollnumber\" class=\"form-control\" ngModel #ref1=\"ngModel\" required>\n                <label *ngIf=\"ref1.invalid && (ref1.dirty || ref1.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n            </div>  \n            <div class=\"form-group\">\n                <label for=\"\">Subject</label>\n                <input type=\"text\" name=\"subject\" class=\"form-control\"  ngModel #ref2=\"ngModel\" required>\n                <label *ngIf=\"ref2.invalid && (ref2.dirty || ref2.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n            </div>  \n            <div class=\"form-group\">\n                <label for=\"\">Marks Acquired</label> \n                <input type=\"number\" name=\"marks\" class=\"form-control\" ngModel #ref3=\"ngModel\" required>\n                <label *ngIf=\"ref3.invalid && (ref3.dirty || ref3.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n            </div>\n            <div class=\"form-group\">\n                <label for=\"\">Total Marks</label>\n                <input type=\"number\" name=\"totalmarks\" class=\"form-control\" ngModel #ref4=\"ngModel\" required>\n                <label *ngIf=\"ref4.invalid && (ref4.dirty || ref4.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n           </div>                    \n           <div class=\"text-center\">\n                <button type=\"submit\" class=\"btn btn-success\">submit</button>\n            </div>       \n    </form>\n    <div *ngIf=\"b1\">\n        <form #ref=\"ngForm\"  (ngSubmit)=\"onsubmit1(ref.value)\">\n            <div>\n                <label>Change Rollno</label>\n                <input type=\"text\" name=\"rollnumber\" class=\"form-control\" [value]=\"objecttomodify1.rollno\" [(ngModel)]=objecttomodify1.rollnumber>\n            </div>\n            <div>\n                <label>Subject</label>\n                <input type=\"text\" name=\"subject\" class=\"form-control\" [value]=\"objecttomodify1.subject\" [(ngModel)]=objecttomodify1.subject>\n            </div>\n            <div>\n                <label>Marks</label>\n                <input type=\"text\" name=\"marks\" class=\"form-control\" [value]=\"objecttomodify1.marks\" [(ngModel)]=objecttomodify1.marks>\n            </div>\n            <div>\n                <label>Total Marks</label>\n                <input type=\"text\" name=\"totalmarks\" class=\"form-control\" [value]=\"objecttomodify1.totalmarks\" [(ngModel)]=objecttomodify1.totalmarks>\n            </div>\n            <div class=\"text-center\"><button type=\"submit\" class=\"btn btn-primary mt-3\">save</button></div>\n        </form>\n    </div>\n</div>\n\n    <div class=\"table table-bordered mt-4\">\n    <table class=\"table table-bordered \">\n       <thead>\n         <th>Roll Number</th>\n        <th>subject</th>\n        <th>Marks</th>\n        <th>Total Marks</th>\n       </thead>\n       <tr *ngFor=\"let std of marks | search:searchterm\">\n            <td>{{std.rollnumber}}</td>\n           <td>{{std.subject}}</td>\n           <td>{{std.marks}}</td>\n           <td>{{std.totalmarks}}</td>\n           <button (click)=\"deleterecord(std.rollnumber)\" class=\"btn btn-danger mr-2\">Delete</button>\n           <button (click)=\"editdata1(std)\" class=\"btn btn-success\">Edit</button>\n       </tr>\n    </table>\n</div>  \n    "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/notification/notification.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin/adminlogin/notification/notification.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row mt-3\">\n    <div class=\"col-md-3\"></div>\n    <div class=\"col-md-6\">\n            <form #ref=\"ngForm\" (ngSubmit)=\"sendnote(ref.value)\">\n        <div class=\"form-group\">\n            <label for=\"\">Notification</label>\n            <input type=\"text\" name=\"notification\"  class=\"form-control\" ngModel>\n            <div class=\"text-center\">\n                <button class=\"btn btn-success mt-3\" type=\"submit\" >send</button>\n            </div>\n        </div>\n        </form>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/receiverequest/receiverequest.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin/adminlogin/receiverequest/receiverequest.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-12\">\n        <ul *ngFor=\"let value of data\">\n            <li><b>Rollnumber:</b>{{value.rollnumber}}</li>\n            <pre style=\"display:flex\"><li class=\"nav-link\">{{value.letter}}</li></pre>\n            <div class=\"row\">\n                <div class=\"col-md-4\"><li class=\"nav-link\"><b>from</b>: {{value.from}}</li></div>\n                <div class=\"col-md-4\"><li class=\"nav-link\"><b>to</b>: {{value.to}}</li></div>\n                <div class=\"col-md-4\"><li class=\"nav-link\"><b>totaldays</b>: {{value.totaldays}}</li></div>\n            </div>\n            <div class=\"text-center\"><button class=\"btn btn-primary mr-2\" (click)=\"accept(value.rollnumber)\">accept</button>\n                <button class=\"btn btn-danger mr-2\" (click)=\"reject()\">reject</button>\n            </div>\n            <div *ngIf=\"c\">\n                <form #ref=\"ngForm\" (ngSubmit)=\"response((ref.value),(value.rollnumber))\">\n                <label for=\"\">Reason for rejection</label>\n                <input type=\"text\" name=\"reason\" id=\"\" ngModel>\n                <div>\n                    <button type=\"submit\" class=\"btn btn-success\"> Submit</button>\n                </div>\n            </form>\n            </div>\n        </ul>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/studentprofile/studentprofile.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin/adminlogin/studentprofile/studentprofile.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"text-center mt-2 mb-2\">\n        <label for=\"\">Search :</label>\n        <input type=\"text\" name=\"search\" placeholder=\"search by rollnumber\" [(ngModel)]=\"searchterm\">\n    </div>\n<table class=\"table table-bordered\">\n<thead>\n    <th>Name</th>\n    <th>Roll no</th>\n    <th>Class</th>\n</thead>\n<tr *ngFor=\"let obj of prof | search:searchterm\" >\n    <td>{{obj.name}}</td>\n    <td>{{obj.rollnumber}}</td>\n    <td>{{obj.class}}</td>\n    \n    <button (click)=\"deleterecord3(obj.rollnumber)\" class=\"btn btn-danger ml-2\">Delete</button>\n</tr>\n</table>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<body>\n <router-outlet></router-outlet>\n</body>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/changepassword/changepassword.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/changepassword/changepassword.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-sm-3\"></div>\n        <div class=\"col-sm-6\">\n            <form  #ref=\"ngForm\" (ngSubmit)=\"changepwd(ref.value)\">\n                    <div class=\"form-group\">\n                            <label for=\"\">Enter User Id</label>\n                            <input type=\"text\" name=\"rollnumber\"  class=\"form-control\" ngModel>\n                        </div>\n                <div class=\"form-group\">\n                    <label for=\"\">Enter New Password</label>\n                    <input type=\"text\" name=\"password\"  class=\"form-control\" ngModel>\n                </div>\n            \n            <div class=\"text-center\">\n                <button class=\"btn btn-success\" type=\"submit\">Submit</button>\n            </div>\n            </form>\n        </div>\n        <div class=\"col-sm-3\"></div>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/home/home.component.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/home/home.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p class=\"mt-4\">Second Campus Learning Solutions is founded by a group of Software Architects from reputed MNCs. Each member of our team has been involved in Tech Industry for more than 15 years. List of MNCs we have been part of, include IBM, INFOSYS, CAPGEMINI, Cognizant, United Health Group, SISL, SIEMENS, Apollo Health Street, CARE ,IDEA OBJECT, TELSTRA , CA Technologies , Motorola, MACH , Lycos and Hewlett-Packard. We have mentored by the team from IISC , IIT , and software companies CTOâ€™s .\n</p>\n<div id=\"demo\" class=\"carousel slide\" data-ride=\"carousel\">\n\n        <!-- Indicators -->\n        <ul class=\"carousel-indicators\">\n          <li data-target=\"#demo\" data-slide-to=\"0\" class=\"active\"></li>\n          <li data-target=\"#demo\" data-slide-to=\"1\"></li>\n          <li data-target=\"#demo\" data-slide-to=\"2\"></li>\n        </ul>\n      \n        <!-- The slideshow -->\n        <div class=\"carousel-inner\">\n          \n          <div class=\"carousel-item active\">\n            <img src=\"http://www.rubycampus.com/wp-content/uploads/2017/01/Benefits-in-Integrating-Study-material-repository-and-Library-Management-System-in-school-management-Software-1-1024x673.jpg\" width=\"1200px\" height=\"500px\" width=\"100%\">\n          </div>\n          <div class=\"carousel-item\">\n            <img src=\"https://image.freepik.com/free-photo/smiling-students-with-backpacks_1098-1220.jpg\" width=\"1200px\" height=\"500px\" width=\"100%\">\n          </div>\n          <div class=\"carousel-item\">\n            <img src=\"https://www.w3webschool.com/blog/wp-content/uploads/2017/07/web-design-training.png\" width=\"1200px\" height=\"500px\" width=\"100%\">\n          </div>\n        </div>\n      \n        <!-- Left and right controls -->\n        <a class=\"carousel-control-prev\" href=\"#demo\" data-slide=\"prev\">\n          <span class=\"carousel-control-prev-icon\"></span>\n        </a>\n        <a class=\"carousel-control-next\" href=\"#demo\" data-slide=\"next\">\n          <span class=\"carousel-control-next-icon\"></span>\n        </a>\n      \n      </div>\n      <div class=\"container mt-4\"><p>Idea Behind Residential Training is\n          Our research into job aspirants among various cities points to data that the average time spent training by a candidate is less than two hours per day, it also points to a general pattern where a candidate idles away hours, months and in a few instances years. We at Second Campus Learning Solutions believe that with our carefully designed curriculum, training structures in addition to our guidance and comfortable environment at our campus will help a candidate achieve their full potential and get placed in three month period. The concept of living and training under one roof was designed to manage students efficiently by steering them away from trivial activities and channelizing a candidate’s time and energy towards gaining employment.\n          \n          Second Campus is a training institue in Hyderabad and offers classroom courses in the areas of Mean Stack Programming to students looking to build their Programing Knowledge.\n          \n          Second Campus Learning Solutions is a fully residential Training Campus for IT job aspirants founded by a group of Software Architects from reputed MNCs. Each member of our team has been involved in Tech Industry for more than 15 years. List of MNCs we have been part of, include; IBM, INFOSYS, CAPGEMINI, Cognizant, United Health Group, SISL, SIEMENS, Apollo Health Street, CARE ,IDEA OBJECT, TELSTRA , CA Technologies , Motorola, MACH , Lycos and Hewlett-Packard.\n          \n          </p>\n        </div>\n      \n\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/login/login.component.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/login/login.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-3\"></div>\n        <div class=\"col-md-6\">\n            <div class=\"text-center\">\n                <h2 class=\"mt-5\">LOGIN</h2>\n            </div>\n            <form #ref=\"ngForm\" (ngSubmit)=\"sendTo(ref.value)\">\n                <div class=\"form-group\">\n                    <label for=\"\">User Id</label>\n                    <input type=\"text\" name=\"rollnumber\" class=\"form-control\" ngModel #ref1=\"ngModel\" required>\n                    <label *ngIf=\"ref1.invalid && (ref1.dirty || ref1.touched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"\" >Password</label>\n                    <input type=\"password\" name=\"password\" class=\"form-control\" ngModel #ref2=\"ngModel\" required minlength=\"5\">\n                    <div *ngIf=\"ref2.invalid && (ref2.dirty || ref2.touched)\">\n                        <div *ngIf=\"ref2.errors.minlength\" class=\"text-danger\">*password must be atleast 5 characters long</div>\n                    </div>\n                </div>\n                <div class=\"text-center\">\n                    <button type=\"submit\" class=\"btn btn-info\">Login</button>                    \n                </div>\n                <div class=\"text-center\">\n                    <a routerLink=\"/main/resetpassword\">Forgot password?</a>\n                </div>         \n                <label *ngIf=\"ref3.invalid && (ref3.dirty || ref3.untouched)\"><div class=\"text-danger\">*this field is manadatory</div></label>\n                <div  class=\"form-check\">\n            <label for=\"f\"  class=\"form-check-label\">\n                <input type=\"radio\" name=\"user\" id=\"f\"  value=\"admin\" class=\"form-check-input\" ngModel #ref3=\"ngModel\" required>\n                Admin\n            </label>\n        </div>\n        <div class=\"form-check\">\n            <label for=\"m\"  class=\"form-check-label\">\n                <input type=\"radio\" name=\"user\" id=\"m\"  value=\"student\"  class=\"form-check-input\" ngModel #ref3=\"ngModel\" required>\n                Student\n            </label>\n        </div>\n            </form>\n        </div>\n        <div class=\"col-md-3\"></div>\n    </div>\n\n</div>\n\n\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/main.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/main.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div><h1 style=\"color: mediumslateblue\"><marquee behavior=\"alternate\" >STUDENT MANAGEMENT SYSTEM</marquee></h1></div>\n    <nav class=\"navbar navbar-expand-sm bg-dark navbar-dark\">\n        <a href=\"\" class=\"navbarbrand\">\n            <img src=\"assets/download.png\" height=\"50%\" width=\"25%\">\n        </a>\n        <button data-toggle=\"collapse\" data-target=\"#n\" class=\"navbar-toggler\">\n            <span class=\"navbar-toggler-icon\"></span>\n        </button>\n        <div id=\"n\" class=\"collapse navbar-collapse justify-content-end text-white\">\n    <ul class=\"navbar-nav \">\n            <li class=\"nav-item\">\n                    <a routerLink='home' class=\"nav-link\">Home</a>\n            </li> \n        <li class=\"nav-item\">\n                <a routerLink='login' class=\"nav-link\">Login</a>\n        </li>   \n    </ul>\n    </div>\n    </nav>\n    <router-outlet></router-outlet>\n    \n    </div>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/otp/otp.component.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/otp/otp.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-sm-3\"></div>\n        <div class=\"col-sm-6\">\n            <form  #ref=\"ngForm\" (ngSubmit)=\"submitotp(ref.value)\">\n                <div class=\"form-group\">\n                    <label for=\"\">Enter OTP</label>\n                    <input type=\"number\" name=\"OTP\"  class=\"form-control\" ngModel>\n                </div>\n            \n            <div class=\"text-center\">\n                <button class=\"btn btn-success\" type=\"submit\">Submit</button>\n            </div>\n            </form>\n        </div>\n        <div class=\"col-sm-3\"></div>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main/resetpassword/resetpassword.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main/resetpassword/resetpassword.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-sm-3\"></div>\n        <div class=\"col-sm-6\">\n            <form  #ref=\"ngForm\" (ngSubmit)=\"sendotp(ref.value)\">\n                <div class=\"form-group\">\n                    <label for=\"\">Enter Id</label>\n                    <input type=\"text\" name=\"rollnumber\"  class=\"form-control\" ngModel>\n                </div>\n            \n            <div class=\"text-center\">\n                <button class=\"btn btn-success\" type=\"submit\">Submit</button>\n            </div>\n            </form>\n        </div>\n        <div class=\"col-sm-3\"></div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/sattendance/sattendance.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/student/studentlogin/sattendance/sattendance.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table class=\"table table-bordered\">\n    <thead>\n        <th>Roll number</th>\n        <th>Month attendace</th>\n        <th>Overall attendace</th>\n    </thead>\n    <tr *ngFor=\"let att of attend\">\n        <td>{{att.rollnumber}}</td>\n        <td>{{att.month}}</td>\n        <td>{{att.overall}}</td>\n    </tr>\n</table>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/sendrequest/sendrequest.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/student/studentlogin/sendrequest/sendrequest.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"text-center\" mt-2>Request to admin</h1>\n<form #ref=\"ngForm\" (ngSubmit)=\"requestof(ref.value)\">\n<div class=\"form-group\">\n<label for=\"\"><h5>Request for leave</h5></label>\n<textarea name=\"letter\" id=\"\" cols=\"30\" rows=\"15\" class=\"form-control\" ngModel></textarea>\n<div class=\"row\">\n<div class=\"col-sm-4\">\n<label for=\"\" class=\"mt-2\"><h6>From</h6></label>\n<input type=\"date\" name=\"from\" class=\"form-control\" ngModel>\n</div> \n<div class=\"col-sm-4\">\n<label for=\"\" class=\"mt-2\"><h6>To</h6></label>\n<input type=\"date\" name=\"to\" class=\"form-control\" ngModel>\n</div>\n<div class=\"col-sm-4\">\n<label for=\"\" class=\"mt-2\"><h6>No of days</h6></label>\n<input type=\"number\" name=\"totaldays\" class=\"form-control\" ngModel>\n</div>\n</div>\n<div class=\"text-center\">\n<button class=\"btn btn-success mt-3\" type=\"submit\">{{b?\"Sendrequest\":\"Request loading\"}}</button>\n</div>\n</div>\n</form>\n<div >\n  <ul *ngFor=\"let value of accept\">\n      <li>{{value.message}}</li>\n    <li>{{value.reason}}</li>\n  </ul>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/sfee/sfee.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/student/studentlogin/sfee/sfee.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table class=\"table table-bordered\">\n        <thead>\n         <th>Roll Number</th>\n         <th>Total Fee</th>\n         <th>Fee paid</th>\n         <th>Fee Due</th>\n        </thead>\n        <tr *ngFor=\"let afee of fees\">\n            <td>{{afee.rollnumber}}</td>\n            <td>{{afee.totalfee}}</td>\n            <td>{{afee.feepaid}}</td>\n            <td>{{afee.feedue}}</td>\n        </tr>\n     </table>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/slogout/slogout.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/student/studentlogin/slogout/slogout.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>slogout works!</p>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/smarks/smarks.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/student/studentlogin/smarks/smarks.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<table class=\"table table-bordered\">\n        <thead>\n         <th>Roll Number</th>\n         <th>subject</th>\n         <th>Marks</th>\n         <th>Total Marks</th>\n        </thead>\n        <tr *ngFor=\"let obj of marks\">\n            <td>{{obj.rollnumber}}</td>\n            <td>{{obj.subject}}</td>\n            <td>{{obj.marks}}</td>\n            <td>{{obj.totalmarks}}</td>\n        </tr>\n     </table>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/snotification/snotification.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/student/studentlogin/snotification/snotification.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n        <div class=\"row\">\n            <div class=\"col-md-3\"></div>\n            <div class=\"col-md-6\">\n                    <div class=\"text-center\">\n<table>\n    <thead>\n        <th><div class=\"blink text-center\"><span >Notification</span></div></th>\n    </thead>\n    <tr *ngFor=\"let std of notify\">\n        <td>{{std.notification}}</td>\n    </tr>\n</table>\n</div>\n</div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/sprofile/sprofile.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/student/studentlogin/sprofile/sprofile.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"text-center \" style=\"color: rgb(255, 21, 0) \">STUDENT PROFILE</h1>\n<table class=\"table table-bordered mt-4\" style=\"width:100%\" *ngFor=\"let obj of prof\">\n   \n        <tr>\n            <th >Name</th>\n            <td>{{obj.name}}</td>\n        </tr>\n        <tr>\n            <th>Roll no</th>\n            <td>{{obj.rollnumber}}</td>\n        </tr>\n        <tr>\n            <th>Class</th>\n            <td>{{obj.class}}</td>\n        </tr>\n    \n    </table>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/studentlogin.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/student/studentlogin/studentlogin.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <nav class=\"navbar navbar-expand-sm bg-dark navbar-dark\">\n        <a href=\"\" class=\"navbarbrand\">\n            <img src=\"assets/download.png\" height=\"50%\" width=\"25%\">\n        </a>\n        <button data-toggle=\"collapse\" data-target=\"#n\" class=\"navbar-toggler\">\n            <span class=\"navbar-toggler-icon\"></span>\n        </button>\n        <div id=\"n\" class=\"collapse navbar-collapse justify-content-end text-white\">\n    <ul class=\"navbar-nav \">\n            <li class=\"nav-item\">\n                    <a routerLink=\"viewprofile\" class=\"nav-link\">Profile</a>\n            </li> \n            <li class=\"nav-item\">\n                    <a routerLink=\"viewmarks\" class=\"nav-link\"> Marks</a>\n            </li> \n        <li class=\"nav-item\">\n                <a routerLink=\"viewattendance\" class=\"nav-link\"> Attendance</a>\n        </li> \n        <li class=\"nav-item\">\n            <a routerLink=\"viewnotification\" class=\"nav-link\"> Notification</a>\n        </li>  \n        <li class=\"nav-item\">\n            <a routerLink=\"viewfee\" class=\"nav-link\"> Fee</a>\n        </li> \n        <li class=\"nav-item\">\n            <a routerLink=\"request\" class=\"nav-link\">Request</a>\n        </li> \n        <li class=\"nav-item\">\n            <a routerLink=\"stdlogout\" class=\"nav-link\">Logout</a>\n        </li> \n         \n    </ul>\n    </div>\n    </nav>\n    <router-outlet></router-outlet>\n    </div>\n\n"

/***/ }),

/***/ "./src/app/admin/admin-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/admin/admin-routing.module.ts ***!
  \***********************************************/
/*! exports provided: AdminRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminRoutingModule", function() { return AdminRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _adminlogin_adminlogin_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./adminlogin/adminlogin.component */ "./src/app/admin/adminlogin/adminlogin.component.ts");
/* harmony import */ var _adminlogin_marks_marks_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./adminlogin/marks/marks.component */ "./src/app/admin/adminlogin/marks/marks.component.ts");
/* harmony import */ var _adminlogin_attendance_attendance_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./adminlogin/attendance/attendance.component */ "./src/app/admin/adminlogin/attendance/attendance.component.ts");
/* harmony import */ var _adminlogin_notification_notification_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./adminlogin/notification/notification.component */ "./src/app/admin/adminlogin/notification/notification.component.ts");
/* harmony import */ var _adminlogin_feestatus_feestatus_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./adminlogin/feestatus/feestatus.component */ "./src/app/admin/adminlogin/feestatus/feestatus.component.ts");
/* harmony import */ var _adminlogin_logout_logout_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./adminlogin/logout/logout.component */ "./src/app/admin/adminlogin/logout/logout.component.ts");
/* harmony import */ var _adminlogin_receiverequest_receiverequest_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./adminlogin/receiverequest/receiverequest.component */ "./src/app/admin/adminlogin/receiverequest/receiverequest.component.ts");
/* harmony import */ var _adminlogin_studentprofile_studentprofile_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./adminlogin/studentprofile/studentprofile.component */ "./src/app/admin/adminlogin/studentprofile/studentprofile.component.ts");
/* harmony import */ var _adminlogin_addstudent_addstudent_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./adminlogin/addstudent/addstudent.component */ "./src/app/admin/adminlogin/addstudent/addstudent.component.ts");












const routes = [{ path: "", component: _adminlogin_adminlogin_component__WEBPACK_IMPORTED_MODULE_3__["AdminloginComponent"],
        children: [
            { path: "studentprofile", component: _adminlogin_studentprofile_studentprofile_component__WEBPACK_IMPORTED_MODULE_10__["StudentprofileComponent"] },
            { path: "addstudent", component: _adminlogin_addstudent_addstudent_component__WEBPACK_IMPORTED_MODULE_11__["AddstudentComponent"] },
            { path: "updatemarks", component: _adminlogin_marks_marks_component__WEBPACK_IMPORTED_MODULE_4__["MarksComponent"] },
            { path: "updateattendance", component: _adminlogin_attendance_attendance_component__WEBPACK_IMPORTED_MODULE_5__["AttendanceComponent"] },
            { path: "updatenotification", component: _adminlogin_notification_notification_component__WEBPACK_IMPORTED_MODULE_6__["NotificationComponent"] },
            { path: "updatefee", component: _adminlogin_feestatus_feestatus_component__WEBPACK_IMPORTED_MODULE_7__["FeestatusComponent"] },
            { path: "receivedrequest", component: _adminlogin_receiverequest_receiverequest_component__WEBPACK_IMPORTED_MODULE_9__["ReceiverequestComponent"] },
            { path: "logout", component: _adminlogin_logout_logout_component__WEBPACK_IMPORTED_MODULE_8__["LogoutComponent"] }
        ]
    }];
let AdminRoutingModule = class AdminRoutingModule {
};
AdminRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AdminRoutingModule);



/***/ }),

/***/ "./src/app/admin/admin.module.ts":
/*!***************************************!*\
  !*** ./src/app/admin/admin.module.ts ***!
  \***************************************/
/*! exports provided: AdminModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminModule", function() { return AdminModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _admin_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./admin-routing.module */ "./src/app/admin/admin-routing.module.ts");
/* harmony import */ var _adminlogin_adminlogin_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./adminlogin/adminlogin.component */ "./src/app/admin/adminlogin/adminlogin.component.ts");
/* harmony import */ var _adminlogin_marks_marks_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./adminlogin/marks/marks.component */ "./src/app/admin/adminlogin/marks/marks.component.ts");
/* harmony import */ var _adminlogin_attendance_attendance_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./adminlogin/attendance/attendance.component */ "./src/app/admin/adminlogin/attendance/attendance.component.ts");
/* harmony import */ var _adminlogin_notification_notification_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./adminlogin/notification/notification.component */ "./src/app/admin/adminlogin/notification/notification.component.ts");
/* harmony import */ var _adminlogin_feestatus_feestatus_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./adminlogin/feestatus/feestatus.component */ "./src/app/admin/adminlogin/feestatus/feestatus.component.ts");
/* harmony import */ var _adminlogin_logout_logout_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./adminlogin/logout/logout.component */ "./src/app/admin/adminlogin/logout/logout.component.ts");
/* harmony import */ var _adminlogin_receiverequest_receiverequest_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./adminlogin/receiverequest/receiverequest.component */ "./src/app/admin/adminlogin/receiverequest/receiverequest.component.ts");
/* harmony import */ var _adminlogin_studentprofile_studentprofile_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./adminlogin/studentprofile/studentprofile.component */ "./src/app/admin/adminlogin/studentprofile/studentprofile.component.ts");
/* harmony import */ var _adminlogin_addstudent_addstudent_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./adminlogin/addstudent/addstudent.component */ "./src/app/admin/adminlogin/addstudent/addstudent.component.ts");
/* harmony import */ var _search_pipe__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./search.pipe */ "./src/app/admin/search.pipe.ts");















let AdminModule = class AdminModule {
};
AdminModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_adminlogin_adminlogin_component__WEBPACK_IMPORTED_MODULE_5__["AdminloginComponent"], _adminlogin_marks_marks_component__WEBPACK_IMPORTED_MODULE_6__["MarksComponent"], _adminlogin_attendance_attendance_component__WEBPACK_IMPORTED_MODULE_7__["AttendanceComponent"], _adminlogin_notification_notification_component__WEBPACK_IMPORTED_MODULE_8__["NotificationComponent"], _adminlogin_feestatus_feestatus_component__WEBPACK_IMPORTED_MODULE_9__["FeestatusComponent"], _adminlogin_logout_logout_component__WEBPACK_IMPORTED_MODULE_10__["LogoutComponent"], _adminlogin_receiverequest_receiverequest_component__WEBPACK_IMPORTED_MODULE_11__["ReceiverequestComponent"], _adminlogin_studentprofile_studentprofile_component__WEBPACK_IMPORTED_MODULE_12__["StudentprofileComponent"], _adminlogin_addstudent_addstudent_component__WEBPACK_IMPORTED_MODULE_13__["AddstudentComponent"], _search_pipe__WEBPACK_IMPORTED_MODULE_14__["SearchPipe"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _admin_routing_module__WEBPACK_IMPORTED_MODULE_4__["AdminRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
        ]
    })
], AdminModule);



/***/ }),

/***/ "./src/app/admin/adminlogin/addstudent/addstudent.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/admin/adminlogin/addstudent/addstudent.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWlubG9naW4vYWRkc3R1ZGVudC9hZGRzdHVkZW50LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin/adminlogin/addstudent/addstudent.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/admin/adminlogin/addstudent/addstudent.component.ts ***!
  \*********************************************************************/
/*! exports provided: AddstudentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddstudentComponent", function() { return AddstudentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let AddstudentComponent = class AddstudentComponent {
    constructor(httpclient) {
        this.httpclient = httpclient;
    }
    ngOnInit() {
    }
    addStudent(x) {
        if (x.rollnumber == "" || x.name == "" || x.password == "" || x.class == "") {
            alert("enter the valid data");
        }
        this.httpclient.post('/admin/save', x).subscribe((res) => {
            alert(res['message']);
        });
    }
};
AddstudentComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
AddstudentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-addstudent',
        template: __webpack_require__(/*! raw-loader!./addstudent.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/addstudent/addstudent.component.html"),
        styles: [__webpack_require__(/*! ./addstudent.component.css */ "./src/app/admin/adminlogin/addstudent/addstudent.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], AddstudentComponent);



/***/ }),

/***/ "./src/app/admin/adminlogin/adminlogin.component.css":
/*!***********************************************************!*\
  !*** ./src/app/admin/adminlogin/adminlogin.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWlubG9naW4vYWRtaW5sb2dpbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/adminlogin/adminlogin.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin/adminlogin/adminlogin.component.ts ***!
  \**********************************************************/
/*! exports provided: AdminloginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminloginComponent", function() { return AdminloginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AdminloginComponent = class AdminloginComponent {
    constructor() { }
    ngOnInit() {
    }
};
AdminloginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-adminlogin',
        template: __webpack_require__(/*! raw-loader!./adminlogin.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/adminlogin.component.html"),
        styles: [__webpack_require__(/*! ./adminlogin.component.css */ "./src/app/admin/adminlogin/adminlogin.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AdminloginComponent);



/***/ }),

/***/ "./src/app/admin/adminlogin/attendance/attendance.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/admin/adminlogin/attendance/attendance.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWlubG9naW4vYXR0ZW5kYW5jZS9hdHRlbmRhbmNlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/admin/adminlogin/attendance/attendance.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/admin/adminlogin/attendance/attendance.component.ts ***!
  \*********************************************************************/
/*! exports provided: AttendanceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AttendanceComponent", function() { return AttendanceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let AttendanceComponent = class AttendanceComponent {
    constructor(service, httpclient, router) {
        this.service = service;
        this.httpclient = httpclient;
        this.router = router;
        this.attd = [];
        this.b = false;
    }
    ngOnInit() {
        this.service.readatt().subscribe(attd => {
            if (attd['message'] == "unauthorized access") {
                alert(attd['message']);
                this.router.navigate(["/main/login"]);
            }
            else {
                this.attd = attd['message'];
            }
        });
    }
    attendance(z) {
        if (z.rollnumber == "" || z.month == "" || z.overall == "") {
            alert("enter the valid data");
        }
        else {
            this.httpclient.post('/admin/saveattd', z).subscribe((res) => {
                alert(res['message']);
            });
            this.service.readatt().subscribe(attd => {
                this.attd = attd['message'];
            });
        }
    }
    deleterecord1(rollno) {
        this.httpclient.delete(`/admin/attend/${rollno}`).subscribe(res => {
            alert(res['message']);
            this.attd = res['data'];
        });
    }
    editdata(attendance) {
        this.objecttomodify = attendance;
        this.b = true;
    }
    onsubmit(modifyobject) {
        this.b = false;
        this.httpclient.put('/admin/modifyatt', modifyobject).subscribe((res) => {
            alert(res['message']);
        });
    }
};
AttendanceComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
AttendanceComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-attendance',
        template: __webpack_require__(/*! raw-loader!./attendance.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/attendance/attendance.component.html"),
        styles: [__webpack_require__(/*! ./attendance.component.css */ "./src/app/admin/adminlogin/attendance/attendance.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], AttendanceComponent);



/***/ }),

/***/ "./src/app/admin/adminlogin/feestatus/feestatus.component.css":
/*!********************************************************************!*\
  !*** ./src/app/admin/adminlogin/feestatus/feestatus.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWlubG9naW4vZmVlc3RhdHVzL2ZlZXN0YXR1cy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/adminlogin/feestatus/feestatus.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/admin/adminlogin/feestatus/feestatus.component.ts ***!
  \*******************************************************************/
/*! exports provided: FeestatusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeestatusComponent", function() { return FeestatusComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let FeestatusComponent = class FeestatusComponent {
    constructor(service, httpclient, router) {
        this.service = service;
        this.httpclient = httpclient;
        this.router = router;
        this.fees = [];
        this.c = false;
    }
    ngOnInit() {
        this.service.readfees().subscribe(fees => {
            if (fees['message'] == "unauthorized access") {
                alert(fees['message']);
                this.router.navigate(["/main/login"]);
            }
            else {
                this.fees = fees['message'];
            }
        });
    }
    fee(y) {
        if (y.rollnumber == "" || y.totalfee == "" || y.feepaid == "" || y.feedue == "") {
            alert("enter the valid data");
        }
        else {
            this.httpclient.post('/admin/savefee', y).subscribe((res) => {
                alert(res['message']);
            });
            this.service.readfees().subscribe(fees => {
                this.fees = fees['message'];
            });
        }
    }
    deleterecord2(rollnumber) {
        this.httpclient.delete(`/admin/fee/${rollnumber}`).subscribe(res => {
            alert(res['message']);
            this.fees = res['data'];
        });
    }
    editdata2(fee) {
        this.objecttomodify2 = fee;
        console.log(this.objecttomodify2);
        this.c = true;
    }
    onsubmit2(modifyobject2) {
        this.c = false;
        this.httpclient.put('/admin/modifyfees', modifyobject2).subscribe((res) => {
            alert(res['message']);
        });
    }
};
FeestatusComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
FeestatusComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-feestatus',
        template: __webpack_require__(/*! raw-loader!./feestatus.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/feestatus/feestatus.component.html"),
        styles: [__webpack_require__(/*! ./feestatus.component.css */ "./src/app/admin/adminlogin/feestatus/feestatus.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], FeestatusComponent);



/***/ }),

/***/ "./src/app/admin/adminlogin/logout/logout.component.css":
/*!**************************************************************!*\
  !*** ./src/app/admin/adminlogin/logout/logout.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWlubG9naW4vbG9nb3V0L2xvZ291dC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/adminlogin/logout/logout.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/admin/adminlogin/logout/logout.component.ts ***!
  \*************************************************************/
/*! exports provided: LogoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutComponent", function() { return LogoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let LogoutComponent = class LogoutComponent {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
        localStorage.clear();
        this.router.navigate(['/main/login']);
    }
};
LogoutComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
LogoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-logout',
        template: __webpack_require__(/*! raw-loader!./logout.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/logout/logout.component.html"),
        styles: [__webpack_require__(/*! ./logout.component.css */ "./src/app/admin/adminlogin/logout/logout.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], LogoutComponent);



/***/ }),

/***/ "./src/app/admin/adminlogin/marks/marks.component.css":
/*!************************************************************!*\
  !*** ./src/app/admin/adminlogin/marks/marks.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWlubG9naW4vbWFya3MvbWFya3MuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/adminlogin/marks/marks.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/admin/adminlogin/marks/marks.component.ts ***!
  \***********************************************************/
/*! exports provided: MarksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MarksComponent", function() { return MarksComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let MarksComponent = class MarksComponent {
    constructor(service, httpclient, router) {
        this.service = service;
        this.httpclient = httpclient;
        this.router = router;
        this.marks = [];
        this.b1 = false;
    }
    ngOnInit() {
        this.service.readmark().subscribe(marks => {
            if (marks['message'] == "unauthorized access") {
                alert(marks['message']);
                this.router.navigate(["/main/login"]);
            }
            else {
                this.marks = marks['message'];
            }
        });
    }
    markson(x) {
        if (x.rollnumber == "" || x.subject == "" || x.marks == "" || x.totalmarks == "") {
            alert("enter the valid data");
        }
        else {
            this.httpclient.post('/admin/savemark', x).subscribe((res) => {
                alert(res['message']);
                this.service.readmark().subscribe(marks => {
                    this.marks = marks['message'];
                });
            });
        }
    }
    deleterecord(rollnumber) {
        this.httpclient.delete(`/admin/delete/${rollnumber}`).subscribe(res => {
            alert(res['message']);
            this.marks = res['data'];
        });
    }
    editdata1(marks) {
        this.objecttomodify1 = marks;
        this.b1 = true;
    }
    onsubmit1(modifyobject) {
        this.b1 = false;
        this.httpclient.put('/admin/modifymark', modifyobject).subscribe((res) => {
            alert(res['message']);
        });
    }
};
MarksComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
MarksComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-marks',
        template: __webpack_require__(/*! raw-loader!./marks.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/marks/marks.component.html"),
        styles: [__webpack_require__(/*! ./marks.component.css */ "./src/app/admin/adminlogin/marks/marks.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], MarksComponent);



/***/ }),

/***/ "./src/app/admin/adminlogin/notification/notification.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/admin/adminlogin/notification/notification.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWlubG9naW4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/adminlogin/notification/notification.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/admin/adminlogin/notification/notification.component.ts ***!
  \*************************************************************************/
/*! exports provided: NotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationComponent", function() { return NotificationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let NotificationComponent = class NotificationComponent {
    constructor(service, httpclient) {
        this.service = service;
        this.httpclient = httpclient;
    }
    ngOnInit() {
    }
    sendnote(m) {
        this.n = m;
        this.httpclient.post('/admin/savenotify', m).subscribe((res) => alert(res['message']));
    }
};
NotificationComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
NotificationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-notification',
        template: __webpack_require__(/*! raw-loader!./notification.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/notification/notification.component.html"),
        styles: [__webpack_require__(/*! ./notification.component.css */ "./src/app/admin/adminlogin/notification/notification.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
], NotificationComponent);



/***/ }),

/***/ "./src/app/admin/adminlogin/receiverequest/receiverequest.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/admin/adminlogin/receiverequest/receiverequest.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWlubG9naW4vcmVjZWl2ZXJlcXVlc3QvcmVjZWl2ZXJlcXVlc3QuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/adminlogin/receiverequest/receiverequest.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/admin/adminlogin/receiverequest/receiverequest.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ReceiverequestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReceiverequestComponent", function() { return ReceiverequestComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let ReceiverequestComponent = class ReceiverequestComponent {
    constructor(service, http, router) {
        this.service = service;
        this.http = http;
        this.router = router;
        this.data = [];
        this.c = false;
    }
    ngOnInit() {
        this.http.get('/admin/readreq').subscribe(data => {
            if (data['message'] == "unauthorized access") {
                alert(data['message']);
                this.router.navigate(["/main/login"]);
            }
            else {
                this.data = data['message'];
            }
        });
    }
    accept(rollnumber) {
        this.loggeduser = this.service.sendloggeduser();
        this.http.post('/admin/savereq', ({ "message": "request is accepted", "rollnumber": rollnumber })).subscribe((res) => {
            alert(res['message']);
        });
    }
    reject() {
        this.c = true;
    }
    response(x, rollnumber) {
        this.c = false;
        x.rollnumber = rollnumber;
        x.message = "Request is rejected";
        this.loggeduser = this.service.sendloggeduser();
        this.http.post('admin/savereq', x).subscribe((res) => {
            alert(res['message']);
            this.http.get('/admin/readreq').subscribe(data => {
                this.data = data['message'];
            });
        });
    }
};
ReceiverequestComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
ReceiverequestComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-receiverequest',
        template: __webpack_require__(/*! raw-loader!./receiverequest.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/receiverequest/receiverequest.component.html"),
        styles: [__webpack_require__(/*! ./receiverequest.component.css */ "./src/app/admin/adminlogin/receiverequest/receiverequest.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], ReceiverequestComponent);



/***/ }),

/***/ "./src/app/admin/adminlogin/studentprofile/studentprofile.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/admin/adminlogin/studentprofile/studentprofile.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWlubG9naW4vc3R1ZGVudHByb2ZpbGUvc3R1ZGVudHByb2ZpbGUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/adminlogin/studentprofile/studentprofile.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/admin/adminlogin/studentprofile/studentprofile.component.ts ***!
  \*****************************************************************************/
/*! exports provided: StudentprofileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentprofileComponent", function() { return StudentprofileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let StudentprofileComponent = class StudentprofileComponent {
    constructor(ds, httpclient, router) {
        this.ds = ds;
        this.httpclient = httpclient;
        this.router = router;
    }
    ngOnInit() {
        this.ds.readData().subscribe(prof => {
            if (prof['message'] == "unauthorized access") {
                alert(prof['message']);
                this.router.navigate(["/main/login"]);
            }
            else {
                this.prof = prof['message'];
            }
        });
    }
    deleterecord3(rollnumber) {
        this.httpclient.delete(`/admin/profile/${rollnumber}`).subscribe(res => {
            alert(res['message']);
            this.prof = res['data'];
        });
    }
};
StudentprofileComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
StudentprofileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-studentprofile',
        template: __webpack_require__(/*! raw-loader!./studentprofile.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin/adminlogin/studentprofile/studentprofile.component.html"),
        styles: [__webpack_require__(/*! ./studentprofile.component.css */ "./src/app/admin/adminlogin/studentprofile/studentprofile.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], StudentprofileComponent);



/***/ }),

/***/ "./src/app/admin/search.pipe.ts":
/*!**************************************!*\
  !*** ./src/app/admin/search.pipe.ts ***!
  \**************************************/
/*! exports provided: SearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPipe", function() { return SearchPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SearchPipe = class SearchPipe {
    transform(data, searchterm) {
        if (!searchterm) {
            return data;
        }
        return data.filter(data => data.rollnumber.toLowerCase().indexOf(searchterm.toLowerCase()) !== -1);
    }
};
SearchPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'search'
    })
], SearchPipe);



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _main_main_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./main/main.component */ "./src/app/main/main.component.ts");
/* harmony import */ var _main_home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./main/home/home.component */ "./src/app/main/home/home.component.ts");
/* harmony import */ var _main_login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./main/login/login.component */ "./src/app/main/login/login.component.ts");
/* harmony import */ var _main_resetpassword_resetpassword_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./main/resetpassword/resetpassword.component */ "./src/app/main/resetpassword/resetpassword.component.ts");
/* harmony import */ var _main_otp_otp_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./main/otp/otp.component */ "./src/app/main/otp/otp.component.ts");
/* harmony import */ var _main_changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./main/changepassword/changepassword.component */ "./src/app/main/changepassword/changepassword.component.ts");









const routes = [{
        path: 'main',
        component: _main_main_component__WEBPACK_IMPORTED_MODULE_3__["MainComponent"],
        children: [
            {
                path: 'home',
                component: _main_home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"]
            },
            { path: 'login',
                component: _main_login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"]
            },
            {
                path: 'resetpassword',
                component: _main_resetpassword_resetpassword_component__WEBPACK_IMPORTED_MODULE_6__["ResetpasswordComponent"]
            },
            {
                path: 'otp',
                component: _main_otp_otp_component__WEBPACK_IMPORTED_MODULE_7__["OtpComponent"]
            },
            {
                path: 'changepassword',
                component: _main_changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_8__["ChangepasswordComponent"]
            }
        ]
    },
    { path: "admin", loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./admin/admin.module */ "./src/app/admin/admin.module.ts")).then(mod => mod.AdminModule) },
    { path: "student", loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./student/student.module */ "./src/app/student/student.module.ts")).then(mod => mod.StudentModule) },
    { path: "", redirectTo: "main/home", pathMatch: "full" }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { useHash: true })],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body{\n    background-image: url(\"http://fc07.deviantart.com/fs39/f/2008/350/0/b/Mint_Vertigo___Full_screen_by_dberg918.png\")\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJib2R5e1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImh0dHA6Ly9mYzA3LmRldmlhbnRhcnQuY29tL2ZzMzkvZi8yMDA4LzM1MC8wL2IvTWludF9WZXJ0aWdvX19fRnVsbF9zY3JlZW5fYnlfZGJlcmc5MTgucG5nXCIpXG59Il19 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'project';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _main_main_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./main/main.component */ "./src/app/main/main.component.ts");
/* harmony import */ var _main_home_home_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./main/home/home.component */ "./src/app/main/home/home.component.ts");
/* harmony import */ var _main_login_login_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./main/login/login.component */ "./src/app/main/login/login.component.ts");
/* harmony import */ var _admin_admin_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./admin/admin.module */ "./src/app/admin/admin.module.ts");
/* harmony import */ var _student_student_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./student/student.module */ "./src/app/student/student.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _authorization_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./authorization.service */ "./src/app/authorization.service.ts");
/* harmony import */ var _main_resetpassword_resetpassword_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./main/resetpassword/resetpassword.component */ "./src/app/main/resetpassword/resetpassword.component.ts");
/* harmony import */ var _main_otp_otp_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./main/otp/otp.component */ "./src/app/main/otp/otp.component.ts");
/* harmony import */ var _main_changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./main/changepassword/changepassword.component */ "./src/app/main/changepassword/changepassword.component.ts");
















let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
            _main_main_component__WEBPACK_IMPORTED_MODULE_6__["MainComponent"],
            _main_home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"],
            _main_login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"],
            _main_resetpassword_resetpassword_component__WEBPACK_IMPORTED_MODULE_13__["ResetpasswordComponent"],
            _main_otp_otp_component__WEBPACK_IMPORTED_MODULE_14__["OtpComponent"],
            _main_changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_15__["ChangepasswordComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _admin_admin_module__WEBPACK_IMPORTED_MODULE_9__["AdminModule"],
            _student_student_module__WEBPACK_IMPORTED_MODULE_10__["StudentModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClientModule"]
        ],
        providers: [{
                provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HTTP_INTERCEPTORS"],
                useClass: _authorization_service__WEBPACK_IMPORTED_MODULE_12__["AuthorizationService"],
                multi: true
            }],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/authorization.service.ts":
/*!******************************************!*\
  !*** ./src/app/authorization.service.ts ***!
  \******************************************/
/*! exports provided: AuthorizationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthorizationService", function() { return AuthorizationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AuthorizationService = class AuthorizationService {
    constructor() { }
    intercept(req, next) {
        //read token from local storage
        const idToken = localStorage.getItem("idToken");
        //if token is found,it adds it to header of request object
        if (idToken) {
            const cloned = req.clone({
                headers: req.headers.set("Authorization", "Bearer " + idToken)
            });
            //and then forward  the request object cloned with token
            return next.handle(cloned);
        }
        else {
            //if token  is not found,forward the same req.object
            return next.handle(req);
        }
    }
};
AuthorizationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], AuthorizationService);



/***/ }),

/***/ "./src/app/login.service.ts":
/*!**********************************!*\
  !*** ./src/app/login.service.ts ***!
  \**********************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let LoginService = class LoginService {
    constructor(hc) {
        this.hc = hc;
        this.isloggedin = true;
    }
    dologin(userobject) {
        return this.hc.post('admin/login', userobject);
    }
    dologinadmin(userobject) {
        return this.hc.post('admin/adminlogin', userobject);
    }
};
LoginService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], LoginService);



/***/ }),

/***/ "./src/app/main/changepassword/changepassword.component.css":
/*!******************************************************************!*\
  !*** ./src/app/main/changepassword/changepassword.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21haW4vY2hhbmdlcGFzc3dvcmQvY2hhbmdlcGFzc3dvcmQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/main/changepassword/changepassword.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/main/changepassword/changepassword.component.ts ***!
  \*****************************************************************/
/*! exports provided: ChangepasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangepasswordComponent", function() { return ChangepasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let ChangepasswordComponent = class ChangepasswordComponent {
    constructor(hc, router) {
        this.hc = hc;
        this.router = router;
    }
    ngOnInit() {
    }
    changepwd(z) {
        this.hc.put('/admin/changepassword', z).subscribe((res) => {
            if (res['message'] == 'password changed') {
                alert(res['message']);
                this.router.navigate(['/main/login']);
            }
            alert(res['message']);
        });
    }
};
ChangepasswordComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
ChangepasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-changepassword',
        template: __webpack_require__(/*! raw-loader!./changepassword.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/changepassword/changepassword.component.html"),
        styles: [__webpack_require__(/*! ./changepassword.component.css */ "./src/app/main/changepassword/changepassword.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], ChangepasswordComponent);



/***/ }),

/***/ "./src/app/main/home/home.component.css":
/*!**********************************************!*\
  !*** ./src/app/main/home/home.component.css ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21haW4vaG9tZS9ob21lLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/main/home/home.component.ts":
/*!*********************************************!*\
  !*** ./src/app/main/home/home.component.ts ***!
  \*********************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HomeComponent = class HomeComponent {
    constructor() { }
    ngOnInit() {
    }
};
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/home/home.component.html"),
        styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/main/home/home.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], HomeComponent);



/***/ }),

/***/ "./src/app/main/login/login.component.css":
/*!************************************************!*\
  !*** ./src/app/main/login/login.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21haW4vbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/main/login/login.component.ts":
/*!***********************************************!*\
  !*** ./src/app/main/login/login.component.ts ***!
  \***********************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/login.service */ "./src/app/login.service.ts");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");






let LoginComponent = class LoginComponent {
    constructor(router, http, loginservice, transfer) {
        this.router = router;
        this.http = http;
        this.loginservice = loginservice;
        this.transfer = transfer;
    }
    ngOnInit() {
    }
    sendTo(x) {
        if (x.rollnumber == "" || x.password == "") {
            alert("please enter the details");
        }
        else {
            if (x.user === "admin") {
                this.loginservice.dologinadmin(x).subscribe(res => {
                    if (res['message'] === "Invalid admin") {
                        alert("invalid admin");
                    }
                    else if (res["message"] === "Invalid admin password") {
                        alert("wrong password");
                    }
                    else if (res["message"] === "admin success") {
                        alert(res["message"]);
                        localStorage.setItem("idToken", res['token']);
                        this.loginservice.isloggedin = true;
                        this.router.navigate(['/admin/studentprofile']);
                    }
                });
            }
            else if (x.user === "student") {
                this.loginservice.dologin(x).subscribe(res => {
                    if (res["message"] === "Invalid username") {
                        alert("invalid user");
                    }
                    if (res["message"] === "Invalid password") {
                        alert("invalid password");
                    }
                    if (res["message"] === "success") {
                        alert("Logged in successfully");
                        localStorage.setItem("idToken", res['token']);
                        this.loginservice.isloggedin = true;
                        this.transfer.loggeduser(res['data']);
                        this.router.navigate(['/student/viewprofile']);
                    }
                });
            }
        }
    }
};
LoginComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: src_app_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"] },
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_5__["TransferService"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/login/login.component.html"),
        styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/main/login/login.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], src_app_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"],
        src_app_transfer_service__WEBPACK_IMPORTED_MODULE_5__["TransferService"]])
], LoginComponent);



/***/ }),

/***/ "./src/app/main/main.component.css":
/*!*****************************************!*\
  !*** ./src/app/main/main.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21haW4vbWFpbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/main/main.component.ts":
/*!****************************************!*\
  !*** ./src/app/main/main.component.ts ***!
  \****************************************/
/*! exports provided: MainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainComponent", function() { return MainComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let MainComponent = class MainComponent {
    constructor() { }
    ngOnInit() {
    }
};
MainComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-main',
        template: __webpack_require__(/*! raw-loader!./main.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/main.component.html"),
        styles: [__webpack_require__(/*! ./main.component.css */ "./src/app/main/main.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], MainComponent);



/***/ }),

/***/ "./src/app/main/otp/otp.component.css":
/*!********************************************!*\
  !*** ./src/app/main/otp/otp.component.css ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21haW4vb3RwL290cC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/main/otp/otp.component.ts":
/*!*******************************************!*\
  !*** ./src/app/main/otp/otp.component.ts ***!
  \*******************************************/
/*! exports provided: OtpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpComponent", function() { return OtpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let OtpComponent = class OtpComponent {
    constructor(hc, router) {
        this.hc = hc;
        this.router = router;
    }
    ngOnInit() {
    }
    submitotp(y) {
        this.hc.post('/admin/verifyotp', y).subscribe((res) => {
            alert(res['message']);
            if (res['message'] == "verifiedOTP") {
                this.router.navigate(['/main/changepassword']);
            }
            else {
                this.router.navigate(['/main/otp']);
            }
        });
    }
};
OtpComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
OtpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-otp',
        template: __webpack_require__(/*! raw-loader!./otp.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/otp/otp.component.html"),
        styles: [__webpack_require__(/*! ./otp.component.css */ "./src/app/main/otp/otp.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], OtpComponent);



/***/ }),

/***/ "./src/app/main/resetpassword/resetpassword.component.css":
/*!****************************************************************!*\
  !*** ./src/app/main/resetpassword/resetpassword.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21haW4vcmVzZXRwYXNzd29yZC9yZXNldHBhc3N3b3JkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/main/resetpassword/resetpassword.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/main/resetpassword/resetpassword.component.ts ***!
  \***************************************************************/
/*! exports provided: ResetpasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetpasswordComponent", function() { return ResetpasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let ResetpasswordComponent = class ResetpasswordComponent {
    constructor(http, router) {
        this.http = http;
        this.router = router;
    }
    ngOnInit() {
    }
    sendotp(x) {
        this.http.post('/admin/resetpassword', x).subscribe((res) => {
            alert(res['message']);
            if (res['message'] == "user found") {
                this.router.navigate(['/main/otp']);
            }
            else {
                this.router.navigate(['/main/resetpassword']);
            }
        });
    }
};
ResetpasswordComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
ResetpasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-resetpassword',
        template: __webpack_require__(/*! raw-loader!./resetpassword.component.html */ "./node_modules/raw-loader/index.js!./src/app/main/resetpassword/resetpassword.component.html"),
        styles: [__webpack_require__(/*! ./resetpassword.component.css */ "./src/app/main/resetpassword/resetpassword.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], ResetpasswordComponent);



/***/ }),

/***/ "./src/app/student/student-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/student/student-routing.module.ts ***!
  \***************************************************/
/*! exports provided: StudentRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentRoutingModule", function() { return StudentRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _student_studentlogin_studentlogin_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../student/studentlogin/studentlogin.component */ "./src/app/student/studentlogin/studentlogin.component.ts");
/* harmony import */ var _studentlogin_sprofile_sprofile_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./studentlogin/sprofile/sprofile.component */ "./src/app/student/studentlogin/sprofile/sprofile.component.ts");
/* harmony import */ var _studentlogin_smarks_smarks_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./studentlogin/smarks/smarks.component */ "./src/app/student/studentlogin/smarks/smarks.component.ts");
/* harmony import */ var _studentlogin_sattendance_sattendance_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./studentlogin/sattendance/sattendance.component */ "./src/app/student/studentlogin/sattendance/sattendance.component.ts");
/* harmony import */ var _studentlogin_snotification_snotification_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./studentlogin/snotification/snotification.component */ "./src/app/student/studentlogin/snotification/snotification.component.ts");
/* harmony import */ var _studentlogin_sfee_sfee_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./studentlogin/sfee/sfee.component */ "./src/app/student/studentlogin/sfee/sfee.component.ts");
/* harmony import */ var _studentlogin_slogout_slogout_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./studentlogin/slogout/slogout.component */ "./src/app/student/studentlogin/slogout/slogout.component.ts");
/* harmony import */ var _studentlogin_sendrequest_sendrequest_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./studentlogin/sendrequest/sendrequest.component */ "./src/app/student/studentlogin/sendrequest/sendrequest.component.ts");











const routes = [{ path: "", component: _student_studentlogin_studentlogin_component__WEBPACK_IMPORTED_MODULE_3__["StudentloginComponent"],
        children: [{ path: "viewprofile", component: _studentlogin_sprofile_sprofile_component__WEBPACK_IMPORTED_MODULE_4__["SprofileComponent"] },
            { path: "viewmarks", component: _studentlogin_smarks_smarks_component__WEBPACK_IMPORTED_MODULE_5__["SmarksComponent"] },
            { path: "viewattendance", component: _studentlogin_sattendance_sattendance_component__WEBPACK_IMPORTED_MODULE_6__["SattendanceComponent"] },
            { path: "viewnotification", component: _studentlogin_snotification_snotification_component__WEBPACK_IMPORTED_MODULE_7__["SnotificationComponent"] },
            { path: "viewfee", component: _studentlogin_sfee_sfee_component__WEBPACK_IMPORTED_MODULE_8__["SfeeComponent"] },
            { path: "stdlogout", component: _studentlogin_slogout_slogout_component__WEBPACK_IMPORTED_MODULE_9__["SlogoutComponent"] },
            { path: "request", component: _studentlogin_sendrequest_sendrequest_component__WEBPACK_IMPORTED_MODULE_10__["SendrequestComponent"] }]
    }];
let StudentRoutingModule = class StudentRoutingModule {
};
StudentRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], StudentRoutingModule);



/***/ }),

/***/ "./src/app/student/student.module.ts":
/*!*******************************************!*\
  !*** ./src/app/student/student.module.ts ***!
  \*******************************************/
/*! exports provided: StudentModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentModule", function() { return StudentModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _student_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./student-routing.module */ "./src/app/student/student-routing.module.ts");
/* harmony import */ var _studentlogin_studentlogin_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./studentlogin/studentlogin.component */ "./src/app/student/studentlogin/studentlogin.component.ts");
/* harmony import */ var _studentlogin_sprofile_sprofile_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./studentlogin/sprofile/sprofile.component */ "./src/app/student/studentlogin/sprofile/sprofile.component.ts");
/* harmony import */ var _studentlogin_smarks_smarks_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./studentlogin/smarks/smarks.component */ "./src/app/student/studentlogin/smarks/smarks.component.ts");
/* harmony import */ var _studentlogin_sattendance_sattendance_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./studentlogin/sattendance/sattendance.component */ "./src/app/student/studentlogin/sattendance/sattendance.component.ts");
/* harmony import */ var _studentlogin_snotification_snotification_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./studentlogin/snotification/snotification.component */ "./src/app/student/studentlogin/snotification/snotification.component.ts");
/* harmony import */ var _studentlogin_sfee_sfee_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./studentlogin/sfee/sfee.component */ "./src/app/student/studentlogin/sfee/sfee.component.ts");
/* harmony import */ var _studentlogin_slogout_slogout_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./studentlogin/slogout/slogout.component */ "./src/app/student/studentlogin/slogout/slogout.component.ts");
/* harmony import */ var _studentlogin_sendrequest_sendrequest_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./studentlogin/sendrequest/sendrequest.component */ "./src/app/student/studentlogin/sendrequest/sendrequest.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");













let StudentModule = class StudentModule {
};
StudentModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_studentlogin_studentlogin_component__WEBPACK_IMPORTED_MODULE_4__["StudentloginComponent"], _studentlogin_sprofile_sprofile_component__WEBPACK_IMPORTED_MODULE_5__["SprofileComponent"], _studentlogin_smarks_smarks_component__WEBPACK_IMPORTED_MODULE_6__["SmarksComponent"], _studentlogin_sattendance_sattendance_component__WEBPACK_IMPORTED_MODULE_7__["SattendanceComponent"], _studentlogin_snotification_snotification_component__WEBPACK_IMPORTED_MODULE_8__["SnotificationComponent"], _studentlogin_sfee_sfee_component__WEBPACK_IMPORTED_MODULE_9__["SfeeComponent"], _studentlogin_slogout_slogout_component__WEBPACK_IMPORTED_MODULE_10__["SlogoutComponent"], _studentlogin_sendrequest_sendrequest_component__WEBPACK_IMPORTED_MODULE_11__["SendrequestComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _student_routing_module__WEBPACK_IMPORTED_MODULE_3__["StudentRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_12__["FormsModule"]
        ]
    })
], StudentModule);



/***/ }),

/***/ "./src/app/student/studentlogin/sattendance/sattendance.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/student/studentlogin/sattendance/sattendance.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnQvc3R1ZGVudGxvZ2luL3NhdHRlbmRhbmNlL3NhdHRlbmRhbmNlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/student/studentlogin/sattendance/sattendance.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/student/studentlogin/sattendance/sattendance.component.ts ***!
  \***************************************************************************/
/*! exports provided: SattendanceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SattendanceComponent", function() { return SattendanceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let SattendanceComponent = class SattendanceComponent {
    constructor(service, router) {
        this.service = service;
        this.router = router;
        this.attend = [];
    }
    ngOnInit() {
        this.user = this.service.sendloggeduser();
        this.service.viewspecifications2(this.user).subscribe(attend => {
            if (attend['message'] == "unauthorized access") {
                alert(attend['message']);
                this.router.navigate(["/main/login"]);
            }
            else {
                this.attend = attend['message'];
            }
        });
    }
};
SattendanceComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
SattendanceComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sattendance',
        template: __webpack_require__(/*! raw-loader!./sattendance.component.html */ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/sattendance/sattendance.component.html"),
        styles: [__webpack_require__(/*! ./sattendance.component.css */ "./src/app/student/studentlogin/sattendance/sattendance.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], SattendanceComponent);



/***/ }),

/***/ "./src/app/student/studentlogin/sendrequest/sendrequest.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/student/studentlogin/sendrequest/sendrequest.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnQvc3R1ZGVudGxvZ2luL3NlbmRyZXF1ZXN0L3NlbmRyZXF1ZXN0LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/student/studentlogin/sendrequest/sendrequest.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/student/studentlogin/sendrequest/sendrequest.component.ts ***!
  \***************************************************************************/
/*! exports provided: SendrequestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendrequestComponent", function() { return SendrequestComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let SendrequestComponent = class SendrequestComponent {
    constructor(service, http, router) {
        this.service = service;
        this.http = http;
        this.router = router;
        this.accept = [];
        this.b = true;
    }
    ngOnInit() {
        this.user = this.service.sendloggeduser();
        this.service.viewspecifications3(this.user).subscribe(accept => {
            if (accept['message'] == "unauthorized access") {
                alert(accept['message']);
                this.router.navigate(["/main/login"]);
            }
            else {
                this.accept = accept['message'];
            }
        });
    }
    requestof(x) {
        this.loggeduser = this.service.sendloggeduser();
        x.rollnumber = this.loggeduser.rollnumber;
        this.http.post('/student/savereq', x).subscribe(res => {
            alert(res["message"]);
            this.user = this.service.sendloggeduser();
            this.service.viewspecifications3(this.user).subscribe(accept => {
                this.accept = accept['message'];
            });
        });
        this.b = false;
    }
};
SendrequestComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
SendrequestComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sendrequest',
        template: __webpack_require__(/*! raw-loader!./sendrequest.component.html */ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/sendrequest/sendrequest.component.html"),
        styles: [__webpack_require__(/*! ./sendrequest.component.css */ "./src/app/student/studentlogin/sendrequest/sendrequest.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], SendrequestComponent);



/***/ }),

/***/ "./src/app/student/studentlogin/sfee/sfee.component.css":
/*!**************************************************************!*\
  !*** ./src/app/student/studentlogin/sfee/sfee.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnQvc3R1ZGVudGxvZ2luL3NmZWUvc2ZlZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/student/studentlogin/sfee/sfee.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/student/studentlogin/sfee/sfee.component.ts ***!
  \*************************************************************/
/*! exports provided: SfeeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SfeeComponent", function() { return SfeeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let SfeeComponent = class SfeeComponent {
    constructor(service, router) {
        this.service = service;
        this.router = router;
        this.fees = [];
    }
    ngOnInit() {
        this.user = this.service.sendloggeduser();
        this.service.viewspecifications1(this.user).subscribe(fees => {
            if (fees['message'] == "unauthorized access") {
                alert(fees['message']);
                this.router.navigate(["/main/login"]);
            }
            else {
                this.fees = fees['message'];
            }
        });
    }
};
SfeeComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
SfeeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sfee',
        template: __webpack_require__(/*! raw-loader!./sfee.component.html */ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/sfee/sfee.component.html"),
        styles: [__webpack_require__(/*! ./sfee.component.css */ "./src/app/student/studentlogin/sfee/sfee.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], SfeeComponent);



/***/ }),

/***/ "./src/app/student/studentlogin/slogout/slogout.component.css":
/*!********************************************************************!*\
  !*** ./src/app/student/studentlogin/slogout/slogout.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnQvc3R1ZGVudGxvZ2luL3Nsb2dvdXQvc2xvZ291dC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/student/studentlogin/slogout/slogout.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/student/studentlogin/slogout/slogout.component.ts ***!
  \*******************************************************************/
/*! exports provided: SlogoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SlogoutComponent", function() { return SlogoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let SlogoutComponent = class SlogoutComponent {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
        localStorage.clear();
        this.router.navigate(['/main/login']);
    }
};
SlogoutComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
SlogoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-slogout',
        template: __webpack_require__(/*! raw-loader!./slogout.component.html */ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/slogout/slogout.component.html"),
        styles: [__webpack_require__(/*! ./slogout.component.css */ "./src/app/student/studentlogin/slogout/slogout.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], SlogoutComponent);



/***/ }),

/***/ "./src/app/student/studentlogin/smarks/smarks.component.css":
/*!******************************************************************!*\
  !*** ./src/app/student/studentlogin/smarks/smarks.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnQvc3R1ZGVudGxvZ2luL3NtYXJrcy9zbWFya3MuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/student/studentlogin/smarks/smarks.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/student/studentlogin/smarks/smarks.component.ts ***!
  \*****************************************************************/
/*! exports provided: SmarksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmarksComponent", function() { return SmarksComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let SmarksComponent = class SmarksComponent {
    constructor(service, router) {
        this.service = service;
        this.router = router;
        this.marks = [];
    }
    ngOnInit() {
        this.user = this.service.sendloggeduser();
        this.service.viewspecifications(this.user).subscribe(marks => {
            if (marks['message'] == "unauthorized access") {
                alert(marks['message']);
                this.router.navigate(["/main/login"]);
            }
            else {
                this.marks = marks['message'];
            }
        });
    }
};
SmarksComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
SmarksComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-smarks',
        template: __webpack_require__(/*! raw-loader!./smarks.component.html */ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/smarks/smarks.component.html"),
        styles: [__webpack_require__(/*! ./smarks.component.css */ "./src/app/student/studentlogin/smarks/smarks.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], SmarksComponent);



/***/ }),

/***/ "./src/app/student/studentlogin/snotification/snotification.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/student/studentlogin/snotification/snotification.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnQvc3R1ZGVudGxvZ2luL3Nub3RpZmljYXRpb24vc25vdGlmaWNhdGlvbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/student/studentlogin/snotification/snotification.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/student/studentlogin/snotification/snotification.component.ts ***!
  \*******************************************************************************/
/*! exports provided: SnotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SnotificationComponent", function() { return SnotificationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");



let SnotificationComponent = class SnotificationComponent {
    constructor(service) {
        this.service = service;
        this.notify = [];
    }
    ngOnInit() {
        this.service.readnote().subscribe(notify => {
            this.notify = notify['message'];
        });
    }
};
SnotificationComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] }
];
SnotificationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-snotification',
        template: __webpack_require__(/*! raw-loader!./snotification.component.html */ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/snotification/snotification.component.html"),
        styles: [__webpack_require__(/*! ./snotification.component.css */ "./src/app/student/studentlogin/snotification/snotification.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"]])
], SnotificationComponent);



/***/ }),

/***/ "./src/app/student/studentlogin/sprofile/sprofile.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/student/studentlogin/sprofile/sprofile.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table, th, td {\n    border: 1px solid black;\n    border-collapse: collapse;\n  }\n  th, td {\n    padding: 5px;\n    text-align: left;    \n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudC9zdHVkZW50bG9naW4vc3Byb2ZpbGUvc3Byb2ZpbGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHVCQUF1QjtJQUN2Qix5QkFBeUI7RUFDM0I7RUFDQTtJQUNFLFlBQVk7SUFDWixnQkFBZ0I7RUFDbEIiLCJmaWxlIjoic3JjL2FwcC9zdHVkZW50L3N0dWRlbnRsb2dpbi9zcHJvZmlsZS9zcHJvZmlsZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUsIHRoLCB0ZCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XG4gICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgfVxuICB0aCwgdGQge1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0OyAgICBcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/student/studentlogin/sprofile/sprofile.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/student/studentlogin/sprofile/sprofile.component.ts ***!
  \*********************************************************************/
/*! exports provided: SprofileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SprofileComponent", function() { return SprofileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/transfer.service */ "./src/app/transfer.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let SprofileComponent = class SprofileComponent {
    constructor(ds, router) {
        this.ds = ds;
        this.router = router;
    }
    ngOnInit() {
        this.user = this.ds.sendloggeduser();
        this.ds.viewspecifications4(this.user).subscribe(prof => {
            if (prof['message'] == "unauthorized access") {
                alert(prof['message']);
                this.router.navigate(["/main/login"]);
            }
            else {
                this.prof = prof['message'];
            }
        });
    }
};
SprofileComponent.ctorParameters = () => [
    { type: src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
SprofileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sprofile',
        template: __webpack_require__(/*! raw-loader!./sprofile.component.html */ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/sprofile/sprofile.component.html"),
        styles: [__webpack_require__(/*! ./sprofile.component.css */ "./src/app/student/studentlogin/sprofile/sprofile.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_transfer_service__WEBPACK_IMPORTED_MODULE_2__["TransferService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], SprofileComponent);



/***/ }),

/***/ "./src/app/student/studentlogin/studentlogin.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/student/studentlogin/studentlogin.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnQvc3R1ZGVudGxvZ2luL3N0dWRlbnRsb2dpbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/student/studentlogin/studentlogin.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/student/studentlogin/studentlogin.component.ts ***!
  \****************************************************************/
/*! exports provided: StudentloginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentloginComponent", function() { return StudentloginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let StudentloginComponent = class StudentloginComponent {
    constructor() { }
    ngOnInit() {
    }
};
StudentloginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-studentlogin',
        template: __webpack_require__(/*! raw-loader!./studentlogin.component.html */ "./node_modules/raw-loader/index.js!./src/app/student/studentlogin/studentlogin.component.html"),
        styles: [__webpack_require__(/*! ./studentlogin.component.css */ "./src/app/student/studentlogin/studentlogin.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], StudentloginComponent);



/***/ }),

/***/ "./src/app/transfer.service.ts":
/*!*************************************!*\
  !*** ./src/app/transfer.service.ts ***!
  \*************************************/
/*! exports provided: TransferService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransferService", function() { return TransferService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let TransferService = class TransferService {
    constructor(httpclient) {
        this.httpclient = httpclient;
    }
    /*senddatafromadmin(b)
  
    {
      console.log(b)
      this.a=b;
  
    }
    senddatatostudents()
    {
      return this.a
    }
    c:any
    
    
      senddatafromaadmin(d)
      {
        console.log(d)
        this.c=d;
      }
      senddatatoastudents()
      {
         return this.c
      }
  
      e:any
      senddatafromattendance(f)
      {
        this.e=f
      }
      senddatatoattendance()
      {
        return this.e
      }
      g:any
      sendnotes(h)
      {
        this.g=h
      }
      senddatatonotification()
      {
        return this.g
      }
  i:any
  
    
  sendtoservice(j)
  {
    this.i=j
    console.log(this.i)
    
  }
  sendtotest2()
  {
    
    return this.i
  }
  
  k:any
  sendtoservice2(l)
  {
    this.k=l
    console.log(this.k)
  }
  sendtotest1()
  {
    return this.k
  }
  m:any
  sendtoservice3(n)
  {
    this.m=n
  }
  sendtotest1reject()
  {
    return this.m
  }
  o:any
  senddataservice(p)
  {
    this.o=p
  }
  senddatastudent()
  {
    return this.o
  }*/
    readData() {
        return this.httpclient.get('admin/readstd');
    }
    readatt() {
        return this.httpclient.get('student/readattd');
    }
    readfees() {
        return this.httpclient.get('student/readfee');
    }
    readmark() {
        return this.httpclient.get('student/readmark');
    }
    readnote() {
        return this.httpclient.get('student/readnote');
    }
    loggeduser(user) {
        this.user = user[0];
    }
    sendloggeduser() {
        return this.user;
    }
    viewspecifications(user) {
        return this.httpclient.post('student/viewspecificmarks', user);
    }
    viewspecifications1(user) {
        return this.httpclient.post('student/viewspecificfees', user);
    }
    viewspecifications2(user) {
        return this.httpclient.post('student/viewspecificattd', user);
    }
    viewspecifications3(user) {
        return this.httpclient.post('student/viewspecificreq', user);
    }
    viewspecifications4(user) {
        return this.httpclient.post('student/viewspecificstd', user);
    }
};
TransferService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
TransferService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], TransferService);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/scls-host12/Desktop/final project/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map