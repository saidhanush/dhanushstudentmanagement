import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }
  sendotp(x) {
    this.http.post('/admin/resetpassword', x).subscribe((res) => {
      alert(res['message'])
      if (res['message'] == "user found") {
        this.router.navigate(['/main/otp'])
      }
      else {
        this.router.navigate(['/main/resetpassword'])
      }
    })
  }

}
