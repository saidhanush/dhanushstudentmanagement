import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LoginService } from 'src/app/login.service';
import { TransferService } from 'src/app/transfer.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private http: HttpClient, private loginservice: LoginService,
    private transfer: TransferService) { }

  ngOnInit() {
  }
  sendTo(x) {
    if (x.rollnumber == "" || x.password == "") {
      alert("please enter the details")
    }
    else {
      if (x.user === "admin") {
        this.loginservice.dologinadmin(x).subscribe(res => {
          if (res['message'] === "Invalid admin") {
            alert("invalid admin")
          }
          else if (res["message"] === "Invalid admin password") {
            alert("wrong password")
          }
          else if (res["message"] === "admin success") {
            alert(res["message"]);
            localStorage.setItem("idToken", res['token'])
            this.loginservice.isloggedin = true;
            this.router.navigate(['/admin/studentprofile'])
          }
        })
      }
      else if (x.user === "student") {

        this.loginservice.dologin(x).subscribe(res => {
          if (res["message"] === "Invalid username") {
            alert("invalid user")
          }
          if (res["message"] === "Invalid password") {
            alert("invalid password")
          }
          if (res["message"] === "success") {
            alert("Logged in successfully")
            localStorage.setItem("idToken", res['token'])
            this.loginservice.isloggedin = true;
            this.transfer.loggeduser(res['data'])
            this.router.navigate(['/student/viewprofile'])
          }
        })
      }
    }
  }
}
