import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sendrequest',
  templateUrl: './sendrequest.component.html',
  styleUrls: ['./sendrequest.component.css']
})
export class SendrequestComponent implements OnInit {
  constructor(private service: TransferService, private http: HttpClient, private router: Router) { }
  accept: any[] = []
  b: boolean = true
  loggeduser;
  user;
  ngOnInit() {
    this.user = this.service.sendloggeduser()
    this.service.viewspecifications3(this.user).subscribe(accept => {
      if (accept['message'] == "unauthorized access") {
        alert(accept['message'])
        this.router.navigate(["/main/login"])
      }
      else {
        this.accept = accept['message']
      }
    })
  }
  requestof(x) {
    this.loggeduser = this.service.sendloggeduser();
    x.rollnumber = this.loggeduser.rollnumber
    this.http.post('/student/savereq', x).subscribe(res => {
      alert(res["message"])
      this.user = this.service.sendloggeduser()
      this.service.viewspecifications3(this.user).subscribe(accept => {
        this.accept = accept['message']
      })
    })
    this.b = false
  }

}
