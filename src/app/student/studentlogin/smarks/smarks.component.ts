import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-smarks',
  templateUrl: './smarks.component.html',
  styleUrls: ['./smarks.component.css']
})
export class SmarksComponent implements OnInit {
  marks: any[] = []
  user: any;
  constructor(private service: TransferService, private router: Router) { }

  ngOnInit() {
    this.user = this.service.sendloggeduser()
    this.service.viewspecifications(this.user).subscribe(marks => {
      if (marks['message'] == "unauthorized access") {
        alert(marks['message'])
        this.router.navigate(["/main/login"])
      }
      else {
        this.marks = marks['message']
      }

    })
  }

} 
