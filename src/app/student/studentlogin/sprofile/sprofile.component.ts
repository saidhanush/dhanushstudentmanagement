import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sprofile',
  templateUrl: './sprofile.component.html',
  styleUrls: ['./sprofile.component.css']
})
export class SprofileComponent implements OnInit {

  constructor(private ds: TransferService, private router: Router) { }

  prof: any[]
  user;
  ngOnInit() {
    this.user = this.ds.sendloggeduser()
    this.ds.viewspecifications4(this.user).subscribe(prof => {
      if (prof['message'] == "unauthorized access") {
        alert(prof['message'])
        this.router.navigate(["/main/login"])
      }
      else {
        this.prof = prof['message']
      }
    })
  }

}
