import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sfee',
  templateUrl: './sfee.component.html',
  styleUrls: ['./sfee.component.css']
})
export class SfeeComponent implements OnInit {
  fees: any[] = [];
  user: any;
  constructor(private service: TransferService, private router: Router) { }

  ngOnInit() {
    this.user = this.service.sendloggeduser()
    this.service.viewspecifications1(this.user).subscribe(fees => {
      if (fees['message'] == "unauthorized access") {
        alert(fees['message'])
        this.router.navigate(["/main/login"])
      }
      else {
        this.fees = fees['message']
      }
    })
  }

}
