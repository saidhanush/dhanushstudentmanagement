import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SfeeComponent } from './sfee.component';

describe('SfeeComponent', () => {
  let component: SfeeComponent;
  let fixture: ComponentFixture<SfeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SfeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SfeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
