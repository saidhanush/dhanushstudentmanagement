import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';

@Component({
  selector: 'app-snotification',
  templateUrl: './snotification.component.html',
  styleUrls: ['./snotification.component.css']
})
export class SnotificationComponent implements OnInit {
  notify: any[] = []
  constructor(private service: TransferService) { }

  ngOnInit() {

    this.service.readnote().subscribe(notify => {
      this.notify = notify['message']
    })
  }

}


