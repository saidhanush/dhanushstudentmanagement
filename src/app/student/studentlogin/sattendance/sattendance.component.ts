import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sattendance',
  templateUrl: './sattendance.component.html',
  styleUrls: ['./sattendance.component.css']
})
export class SattendanceComponent implements OnInit {
  attend: any[] = []
  user: any
  constructor(private service: TransferService, private router: Router) { }

  ngOnInit() {
    this.user = this.service.sendloggeduser()
    this.service.viewspecifications2(this.user).subscribe(attend => {
      if (attend['message'] == "unauthorized access") {
        alert(attend['message'])
        this.router.navigate(["/main/login"])
      }
      else {
        this.attend = attend['message']
      }

    })
  }

}
