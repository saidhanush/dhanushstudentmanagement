import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentloginComponent } from '../student/studentlogin/studentlogin.component';
import { SprofileComponent } from './studentlogin/sprofile/sprofile.component';
import { SmarksComponent } from './studentlogin/smarks/smarks.component';
import { SattendanceComponent } from './studentlogin/sattendance/sattendance.component';
import { SnotificationComponent } from './studentlogin/snotification/snotification.component';
import { SfeeComponent } from './studentlogin/sfee/sfee.component';
import { SlogoutComponent } from './studentlogin/slogout/slogout.component';
import { SendrequestComponent } from './studentlogin/sendrequest/sendrequest.component';
const routes: Routes = [{
  path: "", component: StudentloginComponent,
  children: [{ path: "viewprofile", component: SprofileComponent },
  { path: "viewmarks", component: SmarksComponent },
  { path: "viewattendance", component: SattendanceComponent },
  { path: "viewnotification", component: SnotificationComponent },
  { path: "viewfee", component: SfeeComponent },
  { path: "stdlogout", component: SlogoutComponent },
  { path: "request", component: SendrequestComponent }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
