import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { from, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TransferService {
 a:any
 user:any;
 
  constructor(private httpclient:HttpClient) { }
  /*senddatafromadmin(b)

  {
    console.log(b)
    this.a=b;

  }
  senddatatostudents()
  {
    return this.a
  }
  c:any
  
  
    senddatafromaadmin(d)
    {
      console.log(d)
      this.c=d;
    }
    senddatatoastudents()
    {
       return this.c
    }

    e:any
    senddatafromattendance(f)
    {
      this.e=f
    }
    senddatatoattendance()
    {
      return this.e
    }
    g:any
    sendnotes(h)
    {
      this.g=h
    }
    senddatatonotification()
    {
      return this.g
    }
i:any

  
sendtoservice(j)
{
  this.i=j
  console.log(this.i)
  
}
sendtotest2()
{
  
  return this.i
} 

k:any
sendtoservice2(l)
{
  this.k=l
  console.log(this.k)
}
sendtotest1()
{
  return this.k
}
m:any
sendtoservice3(n)
{
  this.m=n
}
sendtotest1reject()
{
  return this.m
}
o:any
senddataservice(p)
{
  this.o=p
}
senddatastudent()
{
  return this.o
}*/

readData():Observable<any[]>
{
  return this.httpclient.get<any[]>('admin/readstd')
}
readatt():Observable<any[]>
{
  return this.httpclient.get<any[]>('student/readattd')
}
readfees():Observable<any[]>
{
  return this.httpclient.get<any[]>('student/readfee')
}
readmark():Observable<any[]>{
  return this.httpclient.get<any[]>('student/readmark')
}
readnote():Observable<any[]>{
  return this.httpclient.get<any[]>('student/readnote')
}
loggeduser(user){
  this.user=user[0]
}
sendloggeduser(){
  return this.user;
}
viewspecifications(user){
  return this.httpclient.post<any[]>('student/viewspecificmarks',user)
}
viewspecifications1(user){
  return this.httpclient.post<any[]>('student/viewspecificfees',user)
}
viewspecifications2(user){
  return this.httpclient.post<any[]>('student/viewspecificattd',user)
}
viewspecifications3(user)
{ 
  return this.httpclient.post<any[]>('student/viewspecificreq',user)
}
viewspecifications4(user)
{ 
  return this.httpclient.post<any[]>('student/viewspecificstd',user)
}
  }

