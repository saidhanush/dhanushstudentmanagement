import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
isloggedin:boolean=true
  constructor(private hc:HttpClient) { }
  dologin(userobject):Observable<any>
  {
    return this.hc.post<any>('admin/login',userobject)
  }
  dologinadmin(userobject):Observable<any>
  {
    return this.hc.post<any>('admin/adminlogin',userobject)
  }
}
