import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { MarksComponent } from './adminlogin/marks/marks.component';
import { AttendanceComponent } from './adminlogin/attendance/attendance.component';
import { NotificationComponent } from './adminlogin/notification/notification.component';
import { FeestatusComponent } from './adminlogin/feestatus/feestatus.component';
import { LogoutComponent } from './adminlogin/logout/logout.component';
import { ReceiverequestComponent } from './adminlogin/receiverequest/receiverequest.component';
import { StudentprofileComponent } from './adminlogin/studentprofile/studentprofile.component';
import { AddstudentComponent } from './adminlogin/addstudent/addstudent.component';


const routes: Routes = [{
  path: "", component: AdminloginComponent,
  children: [
    { path: "studentprofile", component: StudentprofileComponent },
    { path: "addstudent", component: AddstudentComponent },
    { path: "updatemarks", component: MarksComponent },
    { path: "updateattendance", component: AttendanceComponent },
    { path: "updatenotification", component: NotificationComponent },
    { path: "updatefee", component: FeestatusComponent },
    { path: "receivedrequest", component: ReceiverequestComponent },
    { path: "logout", component: LogoutComponent }

  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
