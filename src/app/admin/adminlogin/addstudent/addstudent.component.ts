import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { from } from 'rxjs';
@Component({
  selector: 'app-addstudent',
  templateUrl: './addstudent.component.html',
  styleUrls: ['./addstudent.component.css']
})
export class AddstudentComponent implements OnInit {

  constructor(private httpclient: HttpClient) { }

  ngOnInit() {
  }
  addStudent(x) {
    if (x.rollnumber == "" || x.name == "" || x.password == "" || x.class == "") {
      alert("enter the valid data")
    }
    this.httpclient.post('/admin/save', x).subscribe((res) => {
      alert(res['message'])
    })
  }

}
