import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  n: any[]
  constructor(private service: TransferService, private httpclient: HttpClient) { }

  ngOnInit() {
  }
  sendnote(m) {

    this.n = m
    this.httpclient.post('/admin/savenotify', m).subscribe((res) =>
      alert(res['message'])
    )
  }

}
