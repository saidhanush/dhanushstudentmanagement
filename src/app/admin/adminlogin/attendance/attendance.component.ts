import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {
  attd: any[] = []
  b: boolean = false
  objecttomodify: object
  searchterm: any
  constructor(private service: TransferService, private httpclient: HttpClient, private router: Router) { }

  ngOnInit() {

    this.service.readatt().subscribe(attd => {
      if (attd['message'] == "unauthorized access") {
        alert(attd['message'])
        this.router.navigate(["/main/login"])
      }
      else {
        this.attd = attd['message']
      }

    })

  }

  attendance(z) {
    if (z.rollnumber == "" || z.month == "" || z.overall == "") {
      alert("enter the valid data")
    }
    else {
      this.httpclient.post('/admin/saveattd', z).subscribe((res) => {
        alert(res['message'])
      })
      this.service.readatt().subscribe(attd => {
        this.attd = attd['message']

      })
    }
  }

  deleterecord1(rollno) {
    this.httpclient.delete(`/admin/attend/${rollno}`).subscribe(res => {
      alert(res['message'])
      this.attd = res['data']
    })
  }
  editdata(attendance) {
    this.objecttomodify = attendance;
    this.b = true
  }
  onsubmit(modifyobject) {
    this.b = false
    this.httpclient.put('/admin/modifyatt', modifyobject).subscribe((res) => {
      alert(res['message'])
    })
  }
}
