import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.css']
})
export class MarksComponent implements OnInit {
  marks: any[] = [];
  b1: boolean = false;
  objecttomodify1: object;
  searchterm: any
  constructor(private service: TransferService, private httpclient: HttpClient, private router: Router) { }

  ngOnInit() {
    this.service.readmark().subscribe(marks => {
      if (marks['message'] == "unauthorized access") {
        alert(marks['message'])
        this.router.navigate(["/main/login"])
      }
      else {
        this.marks = marks['message']
      }

    })
  }
  markson(x) {
    if (x.rollnumber == "" || x.subject == "" || x.marks == "" || x.totalmarks == "") {
      alert("enter the valid data")
    }
    else {
      this.httpclient.post('/admin/savemark', x).subscribe((res) => {
        alert(res['message'])
        this.service.readmark().subscribe(marks => {
          this.marks = marks['message']

        })
      })

    }
  }
  deleterecord(rollnumber) {
    this.httpclient.delete(`/admin/delete/${rollnumber}`).subscribe(res => {
      alert(res['message'])
      this.marks = res['data']
    })
  }
  editdata1(marks) {
    this.objecttomodify1 = marks;
    this.b1 = true
  }
  onsubmit1(modifyobject) {
    this.b1 = false
    this.httpclient.put('/admin/modifymark', modifyobject).subscribe((res) => {
      alert(res['message'])
    })

  }
}
