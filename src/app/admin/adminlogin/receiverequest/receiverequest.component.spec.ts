import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiverequestComponent } from './receiverequest.component';

describe('ReceiverequestComponent', () => {
  let component: ReceiverequestComponent;
  let fixture: ComponentFixture<ReceiverequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiverequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiverequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
