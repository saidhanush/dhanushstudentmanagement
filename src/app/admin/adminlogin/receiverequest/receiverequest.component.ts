import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-receiverequest',
  templateUrl: './receiverequest.component.html',
  styleUrls: ['./receiverequest.component.css']
})
export class ReceiverequestComponent implements OnInit {
  data: any[] = []
  c: boolean = false
  loggeduser;
  constructor(private service: TransferService, private http: HttpClient, private router: Router) { }
  ngOnInit() {
    this.http.get('/admin/readreq').subscribe(data => {
      if (data['message'] == "unauthorized access") {
        alert(data['message'])
        this.router.navigate(["/main/login"])
      }
      else {
        this.data = data['message']
      }
    })
  }
  accept(rollnumber) {
    this.loggeduser = this.service.sendloggeduser();
    this.http.post('/admin/savereq', ({ "message": "request is accepted", "rollnumber": rollnumber })).subscribe((res) => {
      alert(res['message'])
    })
  }
  reject() {
    this.c = true;
  }
  response(x, rollnumber) {

    this.c = false
    x.rollnumber = rollnumber
    x.message = "Request is rejected"
    this.loggeduser = this.service.sendloggeduser();
    this.http.post('admin/savereq', x).subscribe((res) => {
      alert(res['message'])
      this.http.get('/admin/readreq').subscribe(data => {
        this.data = data['message']
      })

    })
  }
}
