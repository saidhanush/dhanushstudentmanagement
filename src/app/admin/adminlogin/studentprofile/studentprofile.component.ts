import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';
import { Subscriber } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-studentprofile',
  templateUrl: './studentprofile.component.html',
  styleUrls: ['./studentprofile.component.css']
})
export class StudentprofileComponent implements OnInit {

  constructor(private ds: TransferService, private httpclient: HttpClient, private router: Router) { }
  prof: any[]
  searchterm: any
  ngOnInit() {
    this.ds.readData().subscribe(prof => {
      if (prof['message'] == "unauthorized access") {
        alert(prof['message'])
        this.router.navigate(["/main/login"])
      }
      else {
        this.prof = prof['message']
      }

    })
  }
  deleterecord3(rollnumber) {
    this.httpclient.delete(`/admin/profile/${rollnumber}`).subscribe(res => {
      alert(res['message'])
      this.prof = res['data']
    })
  }

}
