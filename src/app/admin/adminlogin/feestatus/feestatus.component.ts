import { Component, OnInit } from '@angular/core';
import { TransferService } from 'src/app/transfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-feestatus',
  templateUrl: './feestatus.component.html',
  styleUrls: ['./feestatus.component.css']
})
export class FeestatusComponent implements OnInit {
  fees: any[] = []
  c: boolean = false
  objecttomodify2: object
  searchterm: any
  constructor(private service: TransferService, private httpclient: HttpClient, private router: Router) { }

  ngOnInit() {
    this.service.readfees().subscribe(fees => {
      if (fees['message'] == "unauthorized access") {
        alert(fees['message'])
        this.router.navigate(["/main/login"])
      }
      else {
        this.fees = fees['message']
      }
    })


  }
  fee(y) {
    if (y.rollnumber == "" || y.totalfee == "" || y.feepaid == "" || y.feedue == "") {
      alert("enter the valid data")
    }
    else {
      this.httpclient.post('/admin/savefee', y).subscribe((res) => {
        alert(res['message'])
      })
      this.service.readfees().subscribe(fees => {
        this.fees = fees['message']
      })
    }

  }
  deleterecord2(rollnumber) {
    this.httpclient.delete(`/admin/fee/${rollnumber}`).subscribe(res => {
      alert(res['message'])
      this.fees = res['data']
    })
  }
  editdata2(fee) {
    this.objecttomodify2 = fee;
    console.log(this.objecttomodify2)
    this.c = true
  }
  onsubmit2(modifyobject2) {
    this.c = false
    this.httpclient.put('/admin/modifyfees', modifyobject2).subscribe((res) => {
      alert(res['message'])
    })

  }


}
