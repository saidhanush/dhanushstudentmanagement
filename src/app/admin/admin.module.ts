import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { AdminRoutingModule } from './admin-routing.module';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { MarksComponent } from './adminlogin/marks/marks.component';
import { AttendanceComponent } from './adminlogin/attendance/attendance.component';
import { NotificationComponent } from './adminlogin/notification/notification.component';
import { FeestatusComponent } from './adminlogin/feestatus/feestatus.component';
import { LogoutComponent } from './adminlogin/logout/logout.component';
import { from } from 'rxjs';
import { ReceiverequestComponent } from './adminlogin/receiverequest/receiverequest.component';
import { StudentprofileComponent } from './adminlogin/studentprofile/studentprofile.component';
import { AddstudentComponent } from './adminlogin/addstudent/addstudent.component';
import { SearchPipe } from './search.pipe';
@NgModule({
  declarations: [AdminloginComponent, MarksComponent, AttendanceComponent, NotificationComponent, FeestatusComponent, LogoutComponent, ReceiverequestComponent, StudentprofileComponent, AddstudentComponent, SearchPipe],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule
  ]
})
export class AdminModule { }
